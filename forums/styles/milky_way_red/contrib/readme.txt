===============================================================================

Style name: Milky Way RED
Style Version: 1.6.1
Created by: Mighty Gorgon < http://www.icyphoenix.com >
Modified by: ChriZ (techmaster@seatnet.gr)
Copyright 2009 Mighty Gorgon < http://www.icyphoenix.com >

===============================================================================

Installation:
Upload the milky_way folder into your styles directory and then install it from
ACP.

===============================================================================

You can modify this theme anyway you want as long as my copyright and links
remain in the footer.

You cannot redistribute this theme in whole or in part without my permission.

Custom edits, graphics and changes available at http://www.icyphoenix.com

New styles section:
http://www.icyphoenix.com/styles/styles.php?cat_id=phpbb3

You can customize template colors from this link:
http://www.icyphoenix.com/styles/colorizeit.php

You can customize buttons and ranks from these links:
http://www.icyphoenix.com/customicy_buttons.php
http://www.icyphoenix.com/customicy_ranks.php

Demo customized button:
http://www.icyphoenix.com/customicy/customicy_button.php?bs=milky_way&bf=b_92_26.gif&tc=Testing&tf=bahamas_bold.ttf&ts=12&tcc1=220044&tcc2=ddeeff&tcc3=ffffff&tcc4=ffffff&ct=1&ctb=1

===============================================================================

Notes from ChriZ:

Changes (Milky way to Milky Way Red):
A couple of icon changes (not important enough to mention :) )


Small addition in stylesheet.css: When a subforum has unread posts, it's link name turns red(thanks to <a href="http://www.phpbb.com/community/memberlist.php?mode=viewprofile&u=106440">asinshesq</a> for this addition...
Removed the display of sitename and site description from overall header
Centered sitelogo in overall header and changed its height to 100 pixels
Changed page size to full screen

===============================================================================

Theme History

* 2010/03/01 - Version 1.6.1
- Style updated to phpBB 3.0.7
- Files updated:
	# style.cfg
	# imageset.cfg
	# template.cfg
	# theme.cfg
	# captcha_default.html
	# captcha_qa.html
	# captcha_recaptcha.html
	# editor.js
	# index_body.html
	# login_body.html
	# login_forum.html
	# mcp_ban.html
	# mcp_logs.html
	# mcp_notes_user.html
	# mcp_post.html
	# overall_header.html
	# simple_header.html
	# ucp_pm_viewmessage_print.html
	# ucp_profile_avatar.html
	# viewforum_body.html
	# viewtopic_print.html

* 2009/11/21 - Version 1.6.0
- Style updated to phpBB 3.0.6
- Files updated:
	# style.cfg
	# imageset.cfg
	# template.cfg
	# theme.cfg
	# stylesheet.css
	# breadcrumbs.html
	# breadcrumbs_pages.html
	# captcha_default.html
	# captcha_qa.html
	# captcha_recaptcha.html
	# forumlist_body.html
	# index_body.html
	# login_body.html
	# mcp_forum.html
	# mcp_front.html
	# mcp_logs.html
	# mcp_notes_user.html
	# mcp_post.html
	# mcp_reports.html
	# mcp_topic.html
	# memberlist_im.html
	# memberlist_view.html
	# overall_header.html
	# posting_body.html
	# posting_buttons.html
	# posting_smilies.html
	# quickreply_editor.html
	# report_body.html
	# search_results.html
	# simple_header.html
	# ucp_agreement.html
	# ucp_groups_manage.html
	# ucp_main_drafts.html
	# ucp_main_front.html
	# ucp_main_subscribed.html
	# ucp_pm_history.html
	# ucp_pm_viewfolder.html
	# ucp_pm_viewmessage.html
	# ucp_profile_signature.html
	# ucp_register.html
	# viewtopic_body.html
	# viewtopic_print.html

* 2009/06/03 - Version 1.4.6
- Style updated to phpBB 3.0.5
- Files updated:
	# editor.js
	# index_body.html
	# mcp_notes_user.html
	# mcp_warn_user.html
	# overall_header.html
	# posting_body.html
	# posting_buttons.html
	# posting_review.html
	# posting_topic_review.html
	# ucp_header.html
	# ucp_register.html
	# ucp_zebra_foes.html
	# viewforum_body.html

* 2008/08/26 - Version 1.4.2
- Added full phpBB Copyright and BAN Link
- Files updated:
	# memberlist_view.html
	# overall_footer.html

* 2008/07/11 - Version 1.4.1
- Style updated to phpBB 3.0.2
- Files updated:
	# search_results.html

* 2008/06/02 - Version 1.4.0
- First public release

