/* this file does nothing, if you replace it with the sorttable.js script found in
the contrib folder, you will be able to sort the rsvp data when viewing an event. 

sorttable.js was written by:
  Stuart Langridge, http://www.kryogenix.org/code/browser/sorttable/

It has a different license then the rest of the phpbb forum or the calendar mod
so I'm leaving the sorttable.js file in the contrib folder for users to manually
install as a separate step after they read the separate license info.

  Licenced as X11: http://www.kryogenix.org/code/browser/licence.html

*/

