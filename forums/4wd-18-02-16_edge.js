/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_0',
                            type: 'image',
                            rect: ['0px', '0px', '100%', '100%', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"0.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_1-1',
                            type: 'image',
                            rect: ['0px', '0px', '100%', '100%', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"1-1.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_132',
                            display: 'none',
                            type: 'image',
                            rect: ['0px', '0px', '100%', '100%', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"132.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_8',
                            display: 'none',
                            type: 'image',
                            rect: ['0px', '0px', '100%', '100%', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"8.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_14',
                            display: 'none',
                            type: 'image',
                            rect: ['0px', '0px', '100%', '100%', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"14.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_1-2',
                            type: 'image',
                            rect: ['0px', '0px', '100%', '100%', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"1-2.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_3',
                            display: 'none',
                            type: 'image',
                            rect: ['0px', '0px', '100%', '100%', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"3.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_4',
                            display: 'none',
                            type: 'image',
                            rect: ['0px', '0px', '100%', '100%', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"4.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_5',
                            display: 'none',
                            type: 'image',
                            rect: ['0px', '0px', '100%', '100%', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"5.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_6',
                            display: 'none',
                            type: 'image',
                            rect: ['0px', '0px', '100%', '100%', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"6.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_7',
                            display: 'none',
                            type: 'image',
                            rect: ['0px', '0px', '100%', '100%', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"7.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_9',
                            display: 'none',
                            type: 'image',
                            rect: ['-11px', '0px', '100%', '100%', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"9.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_11',
                            display: 'none',
                            type: 'image',
                            rect: ['5px', '0px', '100%', '100%', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"11.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_12',
                            display: 'none',
                            type: 'image',
                            rect: ['5px', '0px', '100%', '100%', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"12.png",'50%','50%','1900px','60px', 'no-repeat']
                        },
                        {
                            id: '_2',
                            display: 'none',
                            type: 'image',
                            rect: ['6px', '0px', '100%', '100%', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"2.png",'50%','50%','1900px','60px', 'no-repeat']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '100%', '60px', 'auto', 'auto'],
                            sizeRange: ['468px','1900px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 8000,
                    autoPlay: true,
                    labels: {
                        "start": 0
                    },
                    data: [
                        [
                            "eid59",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_8}",
                            'none',
                            'none'
                        ],
                        [
                            "eid126",
                            "opacity",
                            4500,
                            500,
                            "linear",
                            "${_132}",
                            '0',
                            '1'
                        ],
                        [
                            "eid129",
                            "opacity",
                            7500,
                            500,
                            "linear",
                            "${_132}",
                            '1',
                            '0'
                        ],
                        [
                            "eid57",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_4}",
                            'none',
                            'none'
                        ],
                        [
                            "eid50",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_12}",
                            'none',
                            'none'
                        ],
                        [
                            "eid53",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_9}",
                            'none',
                            'none'
                        ],
                        [
                            "eid105",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_14}",
                            'none',
                            'none'
                        ],
                        [
                            "eid107",
                            "display",
                            500,
                            0,
                            "linear",
                            "${_14}",
                            'none',
                            'block'
                        ],
                        [
                            "eid114",
                            "display",
                            4000,
                            0,
                            "linear",
                            "${_14}",
                            'block',
                            'none'
                        ],
                        [
                            "eid51",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_11}",
                            'none',
                            'none'
                        ],
                        [
                            "eid58",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_3}",
                            'none',
                            'none'
                        ],
                        [
                            "eid123",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_132}",
                            'none',
                            'none'
                        ],
                        [
                            "eid124",
                            "display",
                            4500,
                            0,
                            "linear",
                            "${_132}",
                            'none',
                            'block'
                        ],
                        [
                            "eid130",
                            "display",
                            8000,
                            0,
                            "linear",
                            "${_132}",
                            'block',
                            'none'
                        ],
                        [
                            "eid122",
                            "background-position",
                            1000,
                            0,
                            "linear",
                            "${_14}",
                            [50,50],
                            [50,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid54",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_7}",
                            'none',
                            'none'
                        ],
                        [
                            "eid56",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_5}",
                            'none',
                            'none'
                        ],
                        [
                            "eid55",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_6}",
                            'none',
                            'none'
                        ],
                        [
                            "eid60",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_2}",
                            'none',
                            'none'
                        ],
                        [
                            "eid110",
                            "opacity",
                            500,
                            500,
                            "linear",
                            "${_14}",
                            '0',
                            '1'
                        ],
                        [
                            "eid113",
                            "opacity",
                            3500,
                            500,
                            "linear",
                            "${_14}",
                            '1',
                            '0'
                        ]
                    ]
                }
            },
            "Symbol_1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '900px', '60px', 'auto', 'auto'],
                            fill: ['rgba(192,192,192,1)'],
                            id: 'Rectangle',
                            stroke: [0, 'rgba(0,0,0,1)', 'none'],
                            type: 'rect',
                            sizeRange: ['0px', '', '', '']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '900px', '60px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("4wd-18-02-16_edgeActions.js");
})("EDGE-440531694");
