<?php

/**
*
* newspage [Russian]
*
* @package language
* @version $Id$
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'NEWS_TITLE'				=> 'NV Новости',
	'NEWS_CONFIG'				=> 'Настройки',

	'NEWS'						=> 'Новости',
	'NEWS_ARCHIVE_PER_YEAR'				=> 'Ежегодный архивный блок',
	'NEWS_ARCHIVE_PER_YEAR_EXPLAIN'		=> 'Показывает отдельный блок новостей за год',
	'NEWS_ARCHIVE'				=> 'Архив',
	'NEWS_ARCHIVE_OF'			=> 'Архив %s',
	'NEWS_ATTACH_SHOW'			=> 'Показывать вложения',
	'NEWS_ATTACH_SHOW_EXPLAIN'	=> 'Вложения будут отображены на странице новостей.',
	'NEWS_CAT'					=> 'Категории',
	'NEWS_CAT_SHOW'				=> 'Показывать категории',
	'NEWS_CAT_SHOW_EXPLAIN'		=> 'Выбранные форумы будут отображены в качестве категорий.',
	'NEWS_CHAR_LIMIT'			=> 'Количество символов, отображаемых на новостной странице',
	'NEWS_CHAR_LIMIT_EXPLAIN'	=> 'Внимание: это может разделить bb-коды и сделать уродливый вид и невалидный strict HTML.',
	'NEWS_COMMENTS'				=> 'Комментарии',
	'NEWS_FORUMS'				=> 'Форумы, из которых показываются новости.',
	'NEWS_GO_TO_TOPIC'			=> 'Ссылка на тему',
	'NEWS_NONE'					=> 'Нет новостей',
	'NEWS_NUMBER'				=> 'Количество новостей на страницу',
	'NEWS_PAGES'				=> 'Число страниц',
	'NEWS_PAGES_EXPLAIN'		=> 'В архивном виде новости всегда будут отображать столько страниц, сколько они имеют.',
	'NEWS_POLL'					=> 'Опрос',
	'NEWS_POLL_GOTO'			=> 'Нажмите сюда, чтобы проголосовать!',
	'NEWS_POST_BUTTONS'			=> 'Показать кнопки сообщений',
	'NEWS_POST_BUTTONS_EXPLAIN'	=> '(цитата, правка, ...)',
	'NEWS_READ_FULL'			=> 'Читать новости полностью',
	'NEWS_READ_HERE'			=> 'Здесь',
	'NEWS_SAVED'				=> 'Сохранить установки',
	'NEWS_USER_INFO'			=> 'Показать информацию о пользователе',
	'NEWS_USER_INFO_EXPLAIN'	=> '(число сообщений, откуда, ...)',

	'NEWSPAGE'					=> 'NV Новости',
	'INSTALL_NEWSPAGE'			=> 'Установка Новостей',
	'INSTALL_NEWSPAGE_CONFIRM'	=> 'Вы уверены, что хотите установить МОД Новостей?',
	'UPDATE_NEWSPAGE'			=> 'Обновление Новостей',
	'UPDATE_NEWSPAGE_CONFIRM'	=> 'Вы уверены, что хотите обновить МОД Новостей?',
	'UNINSTALL_NEWSPAGE'		=> 'Удаление Новостей',
	'UNINSTALL_NEWSPAGE_CONFIRM'	=> 'Вы уверены, что хотите удалить МОД Новостей?',
));

?>