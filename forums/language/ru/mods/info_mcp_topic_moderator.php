<?php
/**
*
* Topic Moderator MOD mcp [Russian]
*
* @package Topic Moderator MOD language
* @version $Id: info_mcp_topic_moderator.php 0.5.0 2009-06-02 11:00:00 Izya $
* @copyright (c) 2009 Izya (izya@nm.ru)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'MCP_MAIN_TOPIC_MODERATOR'			=>	'Куратор темы',
	
	'TOPIC_MODERATOR_NONE'				=>	'Куратор не назначен',
	'TOPIC_MODERATOR_REMOVE'			=>	'Удалить',
	'TOPIC_MODERATOR_CAN_EDIT'			=>	'Может редактировать сообщения',
	'TOPIC_MODERATOR_CAN_DELETE'		=>	'Может удалять сообщения',
	'TOPIC_MODERATOR_CAN_LOCK_POST'		=>	'Может блокировать сообщения (запрещать редактирование)',
	'TOPIC_MODERATOR_CAN_LOCK_TOPIC'	=>	'Может закрывать/открывать тему',
	'TOPIC_MODERATOR_CAN_REPLY'			=>	'Может отвечать в теме',
));
?>