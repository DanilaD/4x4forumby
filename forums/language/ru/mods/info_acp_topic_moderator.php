<?php
/**
*
* Topic Moderator MOD acp [Russian]
*
* @package Topic Moderator MOD language
* @version $Id: info_acp_topic_moderator.php 0.5.0 2009-06-02 11:00:00 Izya $
* @copyright (c) 2009 Izya (izya@nm.ru)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_TOPIC_MODERATORS'	=>	'Кураторы тем',
	
	'ACP_TOPIC_MODERATORS_CONFIG'		=>	'Конфигурация МОДа Куратор темы',
	'ACP_TOPIC_MODERATORS_LIST'			=>	'Список кураторов',
	
	'ACP_TOPIC_MODERATORS_DISPLAYOPT'	=>	'Настройки отображения',
	'ACP_TOPIC_MDR_IN_VIEWTOPIC'		=>	'Показывать куратора на странице просмотра темы',
	'ACP_TOPIC_MDR_IN_VIEWFORUM'		=>	'Показывать кураторов на странице просмотра форума',
	'ACP_TOPIC_MDR_IF_OWN_TOPIC'		=>	'Показывать кураторов собственных тем',
	'ACP_TOPIC_MDR_IF_OWN_TOPIC_EXP'	=>	'Эта настройка влияет только на страницы просмотра форумов (viewforum)',
	'ACP_TOPIC_MDR_AUTO_MDR_OPT'		=>	'Настройки автоназначения кураторов',
	'ACP_TOPIC_MDR_AUTO_M_FORUMS'		=>	'ID форумов',
	'ACP_TOPIC_MDR_AUTO_M_FORUMS_EXP'	=>	'Укажите ID форумов (через запятую), в которых при создании темы автор автоматически будет становиться куратором',
	'ACP_TOPIC_MDR_AUTO_M_DISP'			=>	'Показывать кураторов на страницах просмотра этих форумов',
	'ACP_TOPIC_MDR_FORUMS_ERROR'		=>	'Неправильно заполнено поле "ID форумов"',
	'ACP_TOPIC_MDR_AUTO_M_PERMS'		=>	'Права доступа',
	'ACP_TOPIC_MDR_AUTO_M_PERMS_EXP'	=>	'Отмеченные права доступа будут присваиваться авторам при создании темы',
	
	'ACP_TOPIC_MDR_PERMS_EXPLAIN'		=>	'Расшифровка прав доступа:<br />r - <strong>r</strong>eply, e - <strong>e</strong>dit, d - <strong>d</strong>elete, p - lock <strong>p</strong>ost, t - lock <strong>t</strong>opic',
));
?>