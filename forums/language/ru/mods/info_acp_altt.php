<?php
/**
*
* @package - NV Advanced Last Topic Titles
* @version $Id: info_acp_altt.php 37 2009-11-18 22:05:28Z nickvergessen $
* @copyright (c) 2007 nickvergessen nickvergessen@gmx.de http://www.flying-bits.org
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
* @localization pack for NV Advanced Last Topic Titles
* @version $Id: info_acp_altt.php 01 2010-05-22 12:58 Mihold $
* @copyright (c) 2010 by Michael Kutsenko mihold@mail.ru
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ALTT_ACTIVE'				=> 'Включить NV advanced last topic titles MOD',

	'ALTT_CHAR_LIMIT'			=> 'количество символов, показываемых на странице',
	'ALTT_CHAR_LIMIT_EXP'		=> 'значения 0 или 64 отключают ограничение длинны текста',
	'ALTT_CONFIG'				=> 'настройка NV advanced last topic titles',
	'ALTT_CONFIG_SAVED'			=> 'изменения сохранены',

	'ALTT_LINK_NAME'			=> 'Текст ссылки это заголовок',
	'ALTT_LINK_URL'				=> 'ссылка ведет к',
	'ALTT_FIRST_POST'			=> 'Первому сообщению в теме',
	'ALTT_LAST_POST'			=> 'Последнему сообщению в теме',
	'ALTT_FIRST_UNREAD_POST'	=> 'Первому непрочитанному сообщению в теме',
	'ALTT_FIRST_UNREAD_POST_NOTE'	=> 'Примечание: Если непрочитанных сообщений нет, то ссылка будет указывать на первое сообщение в теме.',
	'ALTT_POST'					=> 'Сообщения',
	'ALTT_TOPIC'				=> 'Темы',
	'ALTT_LINK_STYLE'			=> 'Стиль ссылки',
	'ALTT_BOLD'					=> 'Жирным',
	'ALTT_ITALIC'				=> 'Наклонным',
	'ALTT_ADV'					=> 'дополнительно:',

	'ALTT_IGNORE_PASSWORD'		=> 'Игнорировать пароль',
	'ALTT_IGNORE_PASSWORD_EXP'	=> 'Заголовок будет показан, даже если форум закрыт паролем.',
	'ALTT_IGNORE_RIGHTS'		=> 'Игнорировать права',
	'ALTT_IGNORE_RIGHTS_EXP'	=> 'Если Вы игнорируете права, то заголовок будет показан даже в том случае когда у пользователя нет прав на просмотр данной темы.',

	'ALTT_TITLE'				=> 'NV advanced last topic titles',

	'NV_ALTT_MOD'					=> '"NV advanced last topic titles" MOD',
	'INSTALL_NV_ALTT_MOD'			=> 'Установка "NV advanced last topic titles" MOD',
	'INSTALL_NV_ALTT_MOD_CONFIRM'	=> 'Выуверены, что хотите установить "NV advanced last topic titles" MOD?',
	'UPDATE_NV_ALTT_MOD'			=> 'Обновление "NV advanced last topic titles" MOD',
	'UPDATE_NV_ALTT_MOD_CONFIRM'	=> 'Выуверены, что хотите обновить "NV advanced last topic titles" MOD?',
	'UNINSTALL_NV_ALTT_MOD'			=> 'Удаление "NV advanced last topic titles" MOD',
	'UNINSTALL_NV_ALTT_MOD_CONFIRM'	=> 'Выуверены, что хотите удалить "NV advanced last topic titles" MOD?',
));

?>