<?php

/**
*
* @package - NV recent topics
* @version $Id: info_acp_recenttopics.php 201 2009-08-03 08:37:12Z nickvergessen $
* @copyright (c) nickvergessen ( http://www.flying-bits.org/ )
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/
if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'RECENT_TOPICS'						=> 'Последние темы, в которых сделаны свежие сообщения',
	'RECENT_TOPICS_LIST'				=> 'Темы с новыми сообщениями добавить в окно "Новые сообщения в темах"',
	'RECENT_TOPICS_LIST_EXPLAIN'		=> 'Выбор данной опции разрешает темам со свежими сообщениями отображаться на главной странице форума в окне "Новые сообщения в темах".',
	'RECENT_TOPICS_MOD'					=> 'Новые сообщения в темах Настройки',

	'RT_CONFIG'							=> 'Настройка',
	'RT_ANTI_TOPICS'					=> 'Исключенные темы',
	'RT_ANTI_TOPICS_EXP'				=> 'Разделить ", " (Например: "7, 9")<br />Для отключения исключения тем установите 0.',
	'RT_NUMBER'							=> 'Новые сообщения в темах',
	'RT_NUMBER_EXP'						=> 'Количество отображаемых тем со свежими сообщениями на одной странице окна на главной странице форума.',
	'RT_PAGE_NUMBER'					=> 'Количество страниц в окне тем со свежими сообщениями',
	'RT_PAGE_NUMBER_EXP'				=> 'Необходимо указать количество отображаемых страниц тем, "1" отключает эту функцию, при "0" отображаются все темы всех форумов (количество страниц определяется автоматически).',
	'RT_PARENTS'						=> 'Показать форум источник',
	'RT_SAVED'							=> 'Настройки сохранены.',

	'RT_VIEW_ON'		=> 'Направить "Новые темы" в ',
	'RT_MEMBERLIST'		=> 'Список пользователей',
	'RT_INDEX'			=> 'Главная страница',
	'RT_SEARCH'			=> 'Поиск',
	'RT_FAQ'			=> 'FAQ',
	'RT_MCP'			=> 'Центр модератора',
	'RT_UCP'			=> 'Мой личный центр',
	'RT_VIEWFORUM'		=> 'Список форумов',
	'RT_VIEWTOPIC'		=> 'Список тем',
	'RT_VIEWONLINE'		=> 'Список тех кто в форуме',
	'RT_POSTING'		=> 'Форму ответа',
	'RT_REPORT'			=> 'Статистику',
	'RT_OTHERS'			=> 'Другой Сайт',

	// Installer
	'INSTALL_RECENT_TOPICS_MOD'				=> 'Install "Recent topics" MOD',
	'INSTALL_RECENT_TOPICS_MOD_CONFIRM'		=> 'Are you sure you want to install the "Recent topics" MOD?',
	'UPDATE_RECENT_TOPICS_MOD'				=> 'Update "Recent topics" MOD',
	'UPDATE_RECENT_TOPICS_MOD_CONFIRM'		=> 'Are you sure you want to update the "Recent topics" MOD?',
	'UNINSTALL_RECENT_TOPICS_MOD'			=> 'Uninstall "Recent topics" MOD',
	'UNINSTALL_RECENT_TOPICS_MOD_CONFIRM'	=> 'Are you sure you want to uninstall the "Recent topics" MOD?',
));

?>