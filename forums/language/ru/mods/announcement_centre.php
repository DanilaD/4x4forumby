<?php
/**
*
* acp_announcements_centre [English]
*
* @package language
* @version $Id: announcement_centre.php 180 2009-01-04 21:09:55Z lefty74 $
* @copyright (c) 2007 lefty74
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/
if (!defined('IN_PHPBB'))
{
	exit;
}


/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// Announcement settings
$lang = array_merge($lang, array(
	'ACP_ANNOUNCEMENT_TITLE'					=> 'Центр управления объявлениями',
	'ACP_ANNOUNCEMENT_TITLE_EXPLAIN'			=> 'С помощью этой страницы вы можете писать объявления для конференции. Также вы можете выбрать, кому показывать объявления. Для гостей можно создавать альтернативные объявления.',
	'ANNOUNCEMENTS_CONFIG'					=> 'Настройка объявлений',

	'ANNOUNCEMENT_ENABLE'					=> 'Включить отображение объявления',
	'ANNOUNCEMENT_ALIGN'					=> 'Выравнивание текста объявления',
	'ANNOUNCEMENT_ALIGN_EXPLAIN'			=> 'Выберите желаемое выравнивание текста объявления.',
	'ANNOUNCEMENT_GUESTS_ALIGN'				=> 'Выравнивание текста объявления для гостей',
	'ANNOUNCEMENT_GUESTS_ALIGN_EXPLAIN'		=> 'Выберите желаемое выравнивание текста объявления для гостей.',
	'ANNOUNCEMENT_SHOW'						=> 'Показывать объявление',
	'ANNOUNCEMENT_SHOW_INDEX'				=> 'Показывать объявление только на главной странице',
	'ANNOUNCEMENT_SHOW_BIRTHDAYS'			=> 'Показывать дни рождения в объявлениях',
	'ANNOUNCEMENT_SHOW_BIRTHDAYS_EXPLAIN'	=> 'Отображение в объявлениях сегодняшних именинников, если функция дней рождений включена. Данная опция имеет приоритет перед основным объявлением.',
	'ANNOUNCEMENT_SHOW_GROUP'				=> 'Выберите группы, которым должно показываться объявление',
	'ANNOUNCEMENT_SHOW_GROUP_EXPLAIN'		=> 'Опция будет работать, если выше выбрать отображение объявления группам.',
	'ANNOUNCEMENT_BIRTHDAY_AVATAR'			=> 'Показывать аватары именинников',
	'ANNOUNCEMENT_BIRTHDAY_AVATAR_EXPLAIN'	=> 'При включении опции вместе с именем будут отображаться аватары именинников.',
	'ANNOUNCEMENT_DRAFT_PREVIEW'			=> 'Предварительный просмотр черновика объявления',
	'ANNOUNCEMENT_TITLE'					=> 'Заголовок объявления',
	'ANNOUNCEMENT_TITLE_EXPLAIN'			=> 'Введите в это поле заголовок объявления. Оставьте поле пустым для использования языковой переменной по умолчанию.',
	'ANNOUNCEMENT_DRAFT'					=> 'Черновик объявления',
	'ANNOUNCEMENT_DRAFT_EXPLAIN'			=> 'Введите в это поле текст объявления',
	'ANNOUNCEMENT_TEXT'						=> 'Текст объявления',
	'ANNOUNCEMENT_TEXT_EXPLAIN'				=> 'Введите в это поле текст объявления.<br/>Введите идентификатор (ID) форума, темы или сообщения для использования первого или последнего сообщения в качестве текста объявления.<br/>Текст объявления заполняется в следующем порядке:<br/>1. ID форума. Если не вводить, то…<br/>2. ID темы. Если не вводить, то…<br/>3. ID сообщения. Если не вводить, то…<br/>4. Текст объявления для гостей.',
	'ANNOUNCEMENT_TEXT_MCP_EXPLAIN'			=> 'Здесь вы можете изменить текст объявления.',
	'ANNOUNCEMENT_TEXT_MCP_EXPLAIN_NOTE'	=> 'Примечание: это возможно только в том случае, если текст объявления берётся не из сообщения.',
	'ANNOUNCEMENT_TEXT_MCP_NOEDIT'			=> 'Изменение объявлений в настоящее время невозможно!',
	'ANNOUNCEMENT_ENABLE_GUESTS'			=> 'Показывать гостям другое объявление',
	'ANNOUNCEMENT_ENABLE_GUESTS_EXPLAIN'	=> 'При включении опции гостям будет показывать другое объявление, за исключение случаев, когда опция отображения объявления выбрана для гостей или для всех.',
	'ANNOUNCEMENT_TITLE_GUESTS'				=> 'Заголовок объявления для гостей',
	'ANNOUNCEMENT_TITLE_GUESTS_EXPLAIN'		=> 'Введите в это поле заголовок объявления для гостей. Оставьте поле пустым для использования языковой переменной по умолчанию.',
	'ANNOUNCEMENT_TEXT_GUESTS'				=> 'Текст объявления для гостей',
	'ANNOUNCEMENT_TEXT_GUESTS_EXPLAIN'		=> 'Введите в это поле текст объявления для гостей.',
	'ACP_ANNOUNCEMENTS_CENTRE'				=> 'Управление объявлениями',
	'ACP_ANNOUNCEMENTS_CENTRE_CONFIG'		=> 'Центр управления объявлениями',

	'MCP_ANNOUNCEMENTS_CENTRE'				=> 'Управление объявлениями',
	'MCP_ANNOUNCE_FRONT'					=> 'Главная страница',

	'ANNOUNCEMENT_UPDATED'					=> 'Объявление обновлено!',
	'ANNOUNCEMENT_GOTOPOST'					=> 'Продолжение…',
	'ANNOUNCEMENT_GOPOST'					=> 'Показывать ссылку на сообщение',
	'ANNOUNCEMENT_GOPOST_EXPLAIN'			=> 'Если в качестве объявления берётся сообщение, то в объявлении будет отображаться ссылка на это сообщение.',

	'COPY_TO_ANNOUNCEMENT_SHORT'			=> 'Копировать в объявление',
	'COPY_TO_GUEST_ANNOUNCEMENT_SHORT'		=> 'Копировать для гостей',
	'COPY_TO_ANNOUNCEMENT'					=> 'Копировать в текст основного объявления',
	'COPY_TO_GUEST_ANNOUNCEMENT'			=> 'Копировать в текст объявления для гостей',

	'ANNOUNCEMENT_SHOW_BIRTHDAYS_AND_ANNOUNCE' 			=> 'Показывать дни рождения и объявления одновременно',
	'ANNOUNCEMENT_SHOW_BIRTHDAYS_AND_ANNOUNCE_EXPLAIN' 	=> 'При включении этой опции дни рождения и текст объявления будут отображаться одновременно. Также при включении данной опции аватары именинников отображаться не будут.',

	'ANNOUNCEMENT_AVA_MAX_SIZE'				=> 'Максимальный размер аватар:',
	'ANNOUNCEMENT_AVA_MAX_SIZE_EXPLAIN'		=> 'Введите максимальный размер аватар именинников.',

	'ANNOUNCEMENT_SHOW_BIRTHDAYS_ALWAYS' 				=> 'Показывать дни рождения в объявлениях, даже если отображение объявления отключено',
	'ANNOUNCEMENT_SHOW_BIRTHDAYS_ALWAYS_EXPLAIN' 		=> 'При включении этой опции дни рождения в объявлениях будут отображаться, даже если отображение объявлений отключено глобально. Эта опция не имеет смысла, если отображение объявлений включено.',

	'ANNOUNCEMENT_FORUM_ID'			=> 'ID форума',
	'ANNOUNCEMENT_TOPIC_ID'			=> 'ID темы',
	'ANNOUNCEMENT_POST_ID'			=> 'ID сообщения',
	'ANNOUNCEMENT_LATEST_POST'		=> 'Последнее сообщение',
	'ANNOUNCEMENT_FIRST_POST'		=> 'Первое сообщение',
	'ANNOUNCEMENT_EVERYONE'			=> 'Всем',
	'ANNOUNCEMENT_GROUPS'			=> 'Группам',
	'ANNOUNCEMENT_GUESTS'			=> 'Гостям',
	'ANNOUNCEMENT_LEFT_ALIGNED'		=> 'По левому краю',
	'ANNOUNCEMENT_CENTER_ALIGNED'	=> 'По центру',
	'ANNOUNCEMENT_RIGHT_ALIGNED'	=> 'По правому краю',

	//Installation vars
	// Installation file stuff, not needed anymore after installation is complete
	'AC_TABLE_CREATED'		=> 	'Таблица управления объявлениями создана.',
	'AC_MODULE_ADDED'		=> 	'Модуль для управления объявлениями добавлен.',
	'AC_CONFIGS_CREATED'	=> 	'Конфигурационные поля заполнены требуемыми данными.',
	'AC_VERSION_UPDATED'	=> 	'Версия Обьявлений обновлена.',
	'AC_INSTALL_COMPLETE'	=> 	'<strong>Установка модуля успешно завершена. Не забудьте удалить папку install!</strong>',
	'AC_INSTALL_RETURN'		=> 	'<br /><br /><br />%sПерейти на главную страницу конференции%s',
	'AC_INSTALL_REDIRECT'	=> 	'Пожалуйста, подождите. Сейчас вы будете переадресованы на страницу завершения установки.',
	'AC_UNINSTALL_REDIRECT'	=> 	'Пожалуйста, подождите. Сейчас вы будете переадресованы на страницу завершения удаления.',

	'AC_PREV_TABLE_DELETE'	=>	'Таблица управления объявлениями удалена.<br />',
	'AC_MODULE_READDED'		=> 	'Модуль для управления объявлениями повторно добавлен.',

	'AC_TABLE_CONFIG_DELETE'	=> 	'Таблица управления объявлениями удалена.<br />',
	'AC_MODULE_DELETED'			=> 	'Модуль для управления объявлениями удалён.<br />',
	'AC_DELETE_COMPLETE'		=> 	'<strong>Модуль успешно удалён. Не забудьте удалить папку install!	</strong>',
	'AC_BACKUP_WARN'			=> 	'Перед продолжением установки не забудьте сделать резервную копию базы данных!',
	'AC_INSTALL_DESC'			=> 	'Данный установочный файл создаст в базе данных новую таблицу и добавит соответствующий модуль.<br />Для продолжения установки щёлкните по ссылке ниже:',
	'AC_UPGRADE_DESC'			=> 	'Данный установочный файл обновит или удалит таблицу базу данных, и добавит или удалит соответствующий модуль.<br />Для продолжения обновления щёлкните по ссылке ниже:',

	'AC_NEW_INSTALL'		=> 	'Новая установка',
	'AC_UPGRADE'			=> 	'Обновление до версии %s',
	'AC_UP_TO_DATE'			=> 	'Установленная версия %s в настоящее время является самой свежей',
	'AC_UNINSTALL'			=> 	'Удаление',
	'AC_PERM_CREATED'		=> 	'Права для управления объявлениями созданы.',


	'AC_DESCRIPTION' 		=>	'Добавляет блок объявления на главную страницу.',
));

?>