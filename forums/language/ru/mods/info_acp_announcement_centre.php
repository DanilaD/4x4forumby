<?php
/**
*
* @package acp
* @version $Id: info_acp_announcement_centre.php 126 2008-10-12 22:10:09Z lefty74 $
* @copyright (c) 2007 lefty74
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

// Create the lang array if it does not already exist
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// Merge language entries into the common lang array
$lang = array_merge($lang, array(
	'ACP_ANNOUNCEMENTS_CENTRE_CONFIG'	=> 'Настройка',
	'ACP_ANNOUNCEMENTS_CENTRE'			=> 'Управление объявлениями',
	'LOG_ANNOUNCEMENT_UPDATED'			=> '<strong>Обновлены объявления</strong>',
	'LOG_ANNOUNCEMENT_CONFIG_UPDATED'	=> '<strong>Обновлены настройки объявлений</strong>',
));
?>