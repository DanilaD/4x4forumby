<?php
/**
*
* groups [Russian]
*
* @package language
* @version $Id: mycalendar3.php,v 0.1Alpha 2009/01/29 13:00:00 Alex1024 Exp $
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/
/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'PAGE_TITLE'		=> 'Форумный календарь событий!',
	'EVENTS_FORUM'		=> 'Разрешить события в календаре',

	'DATE_SQL_FORMAT'	=> '%e %M, %Y',		// This should be changed to the default date format for SQL for your language
	'DATE_INPUT_FORMAT'	=> 'd/m/y',			// Requires 'd', 'm', and 'y' and a punctuation delimiter, order can change

	'INTERVAL'			=> array(
		'DAY'		=> 'день',
		'DAYS'		=> 'день',
		'WEEK'		=> 'неделя',
		'WEEKS'		=> 'неделя',
		'MONTH'		=> 'месяц',
		'MONTHS'	=> 'месяц',
		'YEAR'		=> 'год',
		'YEARS'		=> 'год',
	),

	'WEEKDAY_START'			=> 1,		// First Day of the Week - 0=Sunday, 1=Monday...6=Saturday
	'EVENT_START'			=> 'Однодневное событие или дата начала',
	'EVENT_END'				=> 'Конечная дата и интервал',
	'CALENDAR_ADVANCED'		=> 'дополнительно',
	'CAL_REPEAT_FOREVER'	=> 'ежедневное повторение',
	'CLEAR_DATE'			=> 'Очистить дату',
	'NO_DATE'				=> 'Нет',
	'SELECT_START_DATE'		=> 'Начальная дата', // must escape ' as \\\' for javascript
	'CALENDAR_EVENT'		=> 'Событие в формуный календарь:',
	'PREVIOUS_MONTH'		=> 'Предыдущий месяц',
	'NEXT_MONTH'			=> 'Следующий месяц',
	'PREVIOUS_YEAR'			=> 'Предыдущий год',
	'NEXT_YEAR'				=> 'Следующий год',
	'SEL_INTERVAL'			=> 'Интервал:',
	'CALENDAR_REPEAT'		=> 'Повторить:',
	'DATE_SELECTOR_TITLE'	=> 'Выбор даты',
	'HAPPY'					=> 'ДР:',
	'EVENT'					=> 'События:',
	
	//	Error messages
 'MyCalendar_CantQueryDate' => 'Ошибка запрошенной даты для календаря.',
 'MyCalendar_CantCheckForum' => 'Отказ, тема не может быть добавлена в календарь.',
 'MyCalendar_NoRepeatMult' => 'Не могу определить интервал от даты начала.',
 'MyCalendar_CantInsert' => 'Невозможно вставить новое событие в таблицу календаря.',
 'MyCalendar_CantCheckPreviousEdit' => 'Отказ, нет возможности найти предыдущую дату.',
 'MyCalendar_CantRemoveEvent' => 'Невозможно удалить событие из таблицы календаря.',
 'MyCalendar_CantUpdateEvent' => 'Невозможно обновить событие в таблице календаря.',
 'MyCalendar_CantRelocEvent' => 'Невозможно переместить событие.',
 'MyCalendar_CantCheckLeadPost' => 'Отказ.',
 'MyCalendar_CantDelEvent' => 'Невозможно удалить событие.',
 'MyCalendar_CantCheckDate' => 'Событие добавлено в календарь.',
 'MyCalendar_CantCalcEndDate' => 'Отказ, вычисляя дату окончания.',
 'MyCalendar_CantCheckPreviousEndDate' => 'Проверьте тему. Всё ли праильно указано в датах?',

	//	Mini cal
	'MINI_CAL_CALENDAR'		=> 'Форумный календарь событий',
	'MINI_CAL_ADD_EVENT'	=> 'Добавить событие',
	'MINI_CAL_EVENTS'		=> 'Предстоящие события',
	'MINI_CAL_NO_EVENTS'	=> 'Нет предстоящих событий',
// uses MySQL DATE_FORMAT - %c  long_month, numeric (1..12) - %e  Day of the long_month, numeric (0..31)
// see http://www.mysql.com/doc/D/a/Date_and_time_functions.html for more details
// currently supports: %a, %b, %c, %d, %e, %m, %y, %Y, %H, %k, %h, %l, %i, %s, %p
	'Mini_Cal_date_format'	=> '%a %e %b',
// if you change the first day of the week in constants.php, you should change values for the short day names accordingly
// e.g. FDOW = Sunday -> 	'mini_cal']['day'][1	=> 'Su', ... 	'mini_cal']['day'][7	=> 'Sa', 
//      FDOW = Monday -> 	'mini_cal']['day'][1	=> 'Mo', ... 	'mini_cal']['day'][7	=> 'Su', 
	'MINICAL'	=> array(
		'DAY'	=> array(
			'SHORT'	=> array(
				'Вс',
				'Пн',
				'Вт',
				'Ср',
				'Чт',
				'Пт',
				'Сб'
			), 
			'LONG'	=> array(
				'Воскресенье',
				'Понедельник',
				'Вторник',
				'Среда',
				'Четверг',
				'Пятница',
				'Суббота'
			),
		),
		'MONTH'	=> array(
			'SHORT'	=> array(
				'Янв',
				'Фев',
				'Мар',
				'Апр',
				'Май',
				'Июн',
				'Июл',
				'Авг',
				'Сен',
				'Окт',
				'Ноя',
				'Дек'
			), 
			'LONG'	=> array(
				'Январь', 
				'Февраль',
				'Март',
				'Апрель',
				'Май',
				'Июнь',
				'Июль',
				'Август',
				'Сентябрь',
				'Октябрь',
				'Ноябрь',
				'Декабрь'
			),
		),
	),
));

?>