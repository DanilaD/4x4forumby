<?php
/**
*
* recent_topics [English]
*
* @package language
* @version $Id: recent_topics.php,v 1.0.1 2009/01/10 15:07:24 nissin Exp $
* @copyright (c) 2005 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

// Email settings
$lang = array_merge($lang, array(
	// recent
	'RECENT_NEWS'		=> 'Recent',
	'RECENT_TOPICS'		=> 'Recent topic',
	'RECENT_ANN'		=> 'Recent announcement',
	'RECENT_ANNO_TEXT'	=> 'News',

	// Блок администраторского раздела
	'ACP_RECENT_TOPICS'				=> 'Recent topic',
	'ACP_RECENT_TOPICS_EXPLAIN'		=> 'Here you can change your recent topics information and certain specific options.',
	'RECENT_ENABLE'					=> 'Display recent topics block',
	'RECENT_ANNO_POST'				=> 'Board announcements',
	'RECENT_ANNO_POST_EXPLAIN'		=> 'The text of this post will be shown at the announcements section on the index page of your board.',
	'RECENT_EXCLUDE_FORUM'			=> 'Exclude Forum ID',
	'RECENT_EXCLUDE_FORUM_EXPLAIN'	=> 'Forum we pull the articles from, leave blank to pull from all forums, separate by comma for multi-forums, eg. 1,2,5',
	'RECENT_MAX_TOPIC'				=> 'Limit of recent announcements/hot topics',
	'RECENT_TITLE_LIMIT'			=> 'Character limit for recent topic',
	'RECENT_TITLE_LIMIT_EXPLAIN'	=> '0 means infinite',
));

?>