<?php
/**
*
* @package phpBB3
* @version $Id: functions_mycalendar3.php, v 1.0.3 2009/2/10 01:40 Alf007 Exp $
* 
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

// Port to phpBB3 by Alf007
// Original version:
#################################################################
## Mod Title: mycalendar Mod w/selected forum integration
## Mod Version: 2.2.7
## Auther: mojavelinux <dan@mojavelinux.com>
## Author: marksten
## Description: Enables users to add events to the calendar
##			  through a chosen forum.
#################################################################

/**
 * Check if this is an event forum
 *
 * Query the forums table and determine if the forum requested
 * allows the handling of calendar events.  The results are cache
 * as a static variable.
 *
 * @param  int $forum_id
 *
 * @access public
 * @return boolean
 */
function mycal_forum_check($forum_id) 
{
	global $db, $user;
	$user->setup('mycalendar3');
	// use static variable for caching results
	static $events_forums;

	// if we are not given a forum_id then return false
	if (is_null($forum_id) || $forum_id === '')
	{
		return false;
	}

	if (!isset($events_forums))
	{
		$sql = 'SELECT forum_id, enable_events
				FROM ' . FORUMS_TABLE; 
		if (!$results = $db->sql_query($sql))
		{
			trigger_error('MyCalendar_CantCheckForum');
		}

		while ($row = $db->sql_fetchrow($results))
		{
			$events_forums[$row['forum_id']] = $row['enable_events'];
		}
	}

	return $events_forums[$forum_id] ? $events_forums[$forum_id] : false;
}

/**
 * Enter/delete/modifies the event in the mycalendar table
 *
 * Depending on whether the user chooses new topic or edit post, we
 * make a modification on the mycalendar table to insert or update the event
 *
 * @param  string $mode whether we are editing or posting new topic
 * @param  int	$forum_id sql id of the forum
 * @param  int	$topic_id sql id of the topic
 * @param  int	$post_id sql id of the post
 *
 * @access private
 * @return void
 */
function mycal_submit_event($mode, $forum_id, $topic_id, $post_id)
{
	global $db, $user;

	// Do nothing for a reply/quote
	if ($mode == 'reply' || $mode == 'quote')
	{
		return;
	}

	$user->setup('mycalendar3');

	// setup defaults
	$cal_isodate = mycal_date2iso(utf8_normalize_nfc(request_var('cal_date', $user->lang['NO_DATE'], true)));
	$cal_repeat = 1;

	// get the ending date and interval information
	$cal_interval_date = request_var('cal_interval_date', false);
	$cal_date_end = request_var('cal_date_end', $user->lang['NO_DATE']);
	$cal_repeat_always = request_var('cal_repeat_always', false);

	$cal_interval = request_var('cal_interval', 1);
	$cal_interval_units = request_var('cal_interval_units', 'DAY');

	if ($cal_interval_date && ($cal_date_end != $user->lang['NO_DATE'] || $cal_repeat_always))
	{
		// coax the interval to a postive integer
		$cal_interval = ($tmp_interval = abs($cal_interval)) ? $tmp_interval : 1;
		if ($cal_repeat_always)
		{
			$cal_repeat = 0;
		} else if ($cal_date_end != $user->lang['NO_DATE'])
		{
			$cal_isodate_end = mycal_date2iso($cal_date_end);
			// make sure the end is not before the beginning, if so swap
			if ($cal_isodate_end < $cal_isodate) {
				$tmp = $cal_isodate_end;
				$cal_isodate_end = $cal_isodate;
				$cal_isodate = $tmp;
			}

			// get the number of repeats between the two dates of the interval
			$sql = 'SELECT ';
			if ($cal_interval_units == 'DAY')
			{
				$sql .= "FLOOR((TO_DAYS('$cal_isodate_end') - TO_DAYS('$cal_isodate'))/$cal_interval + 1)";
			} else if ($cal_interval_units == 'WEEK')
			{
				$sql .= "FLOOR((TO_DAYS('$cal_isodate_end') - TO_DAYS('$cal_isodate'))/(7 * $cal_interval) + 1)";
			} else if ($cal_interval_units == 'MONTH')
			{
				$sql .= "FLOOR(PERIOD_DIFF(DATE_FORMAT('$cal_isodate_end', '%Y%m'), DATE_FORMAT('$cal_isodate', '%Y%m'))/$cal_interval + 1)";
			} else if ($cal_interval_units == 'YEAR')
			{
				$sql .= "FLOOR((YEAR('$cal_isodate_end') - YEAR('$cal_isodate'))/$cal_interval + 1)";
			}

			$sql .= ' as cal_repeat';
			if (!$results = $db->sql_query($sql))
			{
				trigger_error('MyCalendar_NoRepeatMult');
			} else if (!$row = $db->sql_fetchrow($results))
			{
				trigger_error('MyCalendar_NoRepeatMult');
			} else if (empty($row['cal_repeat']))
			{
				trigger_error('MyCalendar_NoRepeatMult');
			}

			$cal_repeat = $row['cal_repeat'];
		}
	}

	// if this is a new topic and we can post a date to it (do we have to check this) and
	// we have specified a date, then go ahead and enter it
	if ($mode == 'post' && $cal_isodate && mycal_forum_check($forum_id))
	{
		$sql = 'INSERT INTO ' . 
				   MYCALENDAR_TABLE . ' ' .
				   '(topic_id, cal_date, cal_interval, cal_interval_units, cal_repeat, forum_id) ' .
			   'VALUES ' .
				   "($topic_id, '" . $cal_isodate . "', $cal_interval, '$cal_interval_units', $cal_repeat, $forum_id)";

		if (!$db->sql_query($sql))
		{
			trigger_error('MyCalendar_CantInsert');
		}
	} 
	// if we are editing a post, we either update, insert or delete, depending on if date is set
	// and whether or not a date was specified, so we have to check all that stuff
	else if ($mode == 'edit' && mycal_forum_check($forum_id))
	{
		// check if not allowed to edit the calendar event since this is not the first post
		if (!mycal_first_post($topic_id, $post_id))
		{
			return;
		}

		$sql = 'SELECT ' .
				   'topic_id ' .
			   'FROM ' .
				   MYCALENDAR_TABLE . ' ' .
			   'WHERE ' .
				   "topic_id = $topic_id";

		if (!$result = $db->sql_query($sql))
		{
			trigger_error('MyCalendar_CantCheckPreviousEdit');
		}

		// if we have an event in the calendar for this topic and this is the first post,
		// then we will affect the entry depending on if a date was provided
		if ($db->sql_fetchrow($result))
		{
			// we took away the calendar date (no start date, no date)
			if (!$cal_isodate)
			{
				$sql = 'DELETE FROM ' .
						   MYCALENDAR_TABLE . ' ' .
					   'WHERE ' .
						   "topic_id = $topic_id";
				if (!$db->sql_query($sql))
				{
					trigger_error('MyCalendar_CantRemoveEvent');
				}
			} else
			{
				$sql = 'UPDATE ' .
						   MYCALENDAR_TABLE . ' ' .
					   'SET ' .
						   "cal_date = '" . $cal_isodate . "', " .
						   "cal_interval = $cal_interval, " .
						   "cal_interval_units = '$cal_interval_units', " .
						   "cal_repeat = $cal_repeat " .
					   'WHERE ' .
						   "topic_id = $topic_id";
				if (!$db->sql_query($sql))
				{
					trigger_error('MyCalendar_CantUpdateEvent');
				}
			}
		}
		// insert the new entry if a date was provided
		else if ($cal_isodate)
		{
			$sql = 'INSERT INTO ' . 
					   MYCALENDAR_TABLE .
					   ' (topic_id, cal_date, cal_interval, cal_interval_units, cal_repeat, forum_id) ' .
				   'VALUES ' .
					   "($topic_id, '" . $cal_isodate . "', $cal_interval, '$cal_interval_units', $cal_repeat, $forum_id)";

			if (!$db->sql_query($sql))
			{
				trigger_error('MyCalendar_CantInsert');
			}
		}
	}
}

function mycal_move_event($new_forum_id, $topic_id, $leave_shadow = false)
{
	global $db, $user;
	$user->setup('mycalendar3');
	
	// if we are not leaving a shadow and the new forum doesn't do events,
	// then delete to event and return
	if (!$leave_shadow && !mycal_forum_check($new_forum_id))
	{
		if (!$leave_shadow)
		{
			mycal_delete_event($topic_id, null, false);
			return;
		}
	} else
	{
		$sql = 'UPDATE ' .
				   MYCALENDAR_TABLE . ' ' .
			   'SET ' .
				   "forum_id = $new_forum_id " .
			   'WHERE ' .
				   "topic_id IN ($topic_id)";
		if (!$db->sql_query($sql))
		{
			trigger_error('MyCalendar_CantRelocEvent'); 
		}
	}
}

/**
 * Remove an event from the mycalendar table
 *
 * When a topic is removed, and not just a reply within that topic,
 * the event is deleted from the mycalendar table appropriately.
 *
 * @param  mixed  $topic_id either int or comma seperated..if $check_post is false, it must be int
 * @param  int  $post_id
 * @param  bool $check_post verify that the post is the first post and not a reply
 *
 * @access public
 * @return void
 */
/*
 *  Delete action in includes/functions_admin.php@delete_topics()
 *
function mycal_delete_event($topic_id, $post_id, $check_post = false) 
{
	global $db;

	// First we must verify that this we are deleting a whole topic...not
	// just a single post within the topic
	// we have to use two queries for old databases, even though MySQL can do it in one
	if ($check_post)
	{
		$sql = 'SELECT ' .
				   'c.cal_id ' .
			   'FROM ' . 
				   MYCALENDAR_TABLE . ' as c, ' .
				   TOPICS_TABLE . ' as t ' .
			   'WHERE ' .
				   "t.topic_id = $topic_id AND " .
				   "c.topic_id = $topic_id AND " .
				   "t.topic_first_post_id = $post_id";
		if (!$result = $db->sql_query($sql))
		{
			trigger_error('MyCalendar_CantCheckLeadPost');
		}
	}

	if (!$check_post || $db->sql_numrows($result) > 0)
	{
		$sql = 'DELETE FROM ' .
				   MYCALENDAR_TABLE . ' ' .
			   'WHERE ' . 
				   "topic_id IN ($topic_id)";

		if (!$db->sql_query($sql))
		{
			trigger_error('MyCalendar_CantDelEvent'); 
		}
	}
}*/

/**
 * Generate the event date info for topic header view
 *
 * This function will generate a string for the first post in a topic
 * that declares an event date, but only if the event date has a reference
 * to a forum which allows events to be used.  In the case of a reoccuring/block date,
 * the display will be such that it explains this attribute.
 *
 * @param int  $topic_id identifier of the topic
 * @param int  $post_id identifier of the post, used to determine if this is the leading post (or 0 for forum view)
 *
 * @access public
 * @return string body message
 */
function mycal_show_event($topic_id, $post_id) 
{
	global $db, $user;
	$user->setup('mycalendar3');
	$format = $user->lang['DATE_SQL_FORMAT'];
	$info = '';
	$sql = 'SELECT ' .
			   'c.cal_date, ' .
			   'DATE_FORMAT(c.cal_date, "' . $format . '") as cal_date_f, ' .
			   'c.cal_interval, ' .
			   'c.cal_interval_units, ' .
			   'c.cal_repeat, ' .
			   'DATE_FORMAT(c.cal_date + INTERVAL (c.cal_repeat - 1) DAY, "%M %D, %Y") as cal_date_end ' .
		   'FROM ' . 
			   MYCALENDAR_TABLE . ' as c, ' .
			   TOPICS_TABLE . ' as t, ' .
			   FORUMS_TABLE . ' as f ' .
			'WHERE ' .
				"t.topic_id = $topic_id AND " .
				"c.topic_id = $topic_id AND " .
                'c.forum_id = f.forum_id AND f.enable_events > 0';
	if ($post_id != 0)
	{
		$sql .= ' AND t.topic_first_post_id = ' . $post_id;
	}
	if (!$result = $db->sql_query($sql))
	{
		trigger_error('MyCalendar_CantCheckDate');
	}

	// we found a calendar event, so let's append it
	$row = $db->sql_fetchrow($result);
	if ($row)
	{
		$cal_date = $row['cal_date'];
		$cal_date_f = $row['cal_date_f'];
		$cal_interval = $row['cal_interval'];
		$cal_repeat = $row['cal_repeat'];
		$event['message'] = '<i>' . mycal_translate_date($cal_date_f) . '</i>';
		// if this is more than a single date event, then dig deeper
		if ($row['cal_repeat'] != 1)
		{
			// if this is a repeating or block event (repeat > 1), show end date!
			if ($row['cal_repeat'] > 1)
			{
				// have to pull a little tweak on WEEK since the interval doesn't exist in SQL
				$units = $row['cal_interval_units'] == 'WEEK' ? '* 7 DAY' : $row['cal_interval_units'];
				// we have to do another query here because SQL does not allow interval units to be a field variable
				$sql2 = 'SELECT DATE_FORMAT(\'' . $cal_date . '\' + INTERVAL ' . $cal_interval . ' * (' . $cal_repeat . ' - 1) ' . $units . ', "' . $format . '") as cal_date_end ';
				if (!$results2 = $db->sql_query($sql2))
				{
					trigger_error('MyCalendar_CantCalcEndDate');
				}
				$row2 = $db->sql_fetchrow($results2);
				$cal_date_end = $row2['cal_date_end'];
				$event['message'] .= ' - <i>' . mycal_translate_date($cal_date_end) . '</i>';
			}

			// we have a non-continuous interval or a 'forever' repeating event, so we will explain it
			if (!($row['cal_interval_units'] == 'DAY' && $cal_interval == 1 && $cal_repeat != 0))
			{
				$units = ($row['cal_interval'] == 1) ? $user->lang['INTERVAL'][strtoupper($row['cal_interval_units'])] : $user->lang['INTERVAL'][strtoupper($row['cal_interval_units']) . 'S'];
				$repeat = $row['cal_repeat'] ? $row['cal_repeat'] . 'x' : $user->lang['CAL_REPEAT_FOREVER'];
				$event['message'] .= '<br /><b>' .  $user->lang['SEL_INTERVAL'] .  '</b> ' .  $row['cal_interval'] .  ' ' .  $units .  '<br /><b>' .  $user->lang['CALENDAR_REPEAT'] .  '</b> ' .  $repeat;
			}
		}
        $info = $event['message'];
	} 

	return $info;
}

/**
 * Print out the selection box for selecting date
 *
 * When a new post is added or the first post in topic is edited, the poster
 * will be presented with an event date selection box if posting to an event forum
 *
 * @param  string $mode
 * @param  int $topic_id
 * @param  int $post_id
 * @param  int $forum_id
 * @param  object $template
 *
 * @access private
 * @return void
 */
function mycal_generate_entry($mode, $forum_id, $topic_id, $post_id, &$template) 
{
	global $db, $user;

	// if this is a reply/quote or not an event forum or if we are editing and it is not the first post, just return
	if ($mode == 'reply' || $mode == 'quote' || !mycal_forum_check($forum_id) || ($mode == 'edit' && !mycal_first_post($topic_id, $post_id)))
	{
		return;
	}

	$user->setup('mycalendar3');

	// set up defaults first in case we don't find any event information (such as a new post)
	$cal_interval = 1;
	$cal_interval_units = 'DAY';
	$cal_repeat_always = '';

	// we only want to get the date if this is an edit fresh, and not preview
	$cal_date = request_var('cal_date', $user->lang['NO_DATE']);
	$cal_date_end = request_var('cal_date_end', $user->lang['NO_DATE']);
	// okay we are starting an edit on the post, let's get the required info from the tables
	if (($cal_date == $user->lang['NO_DATE'] || $cal_date_end == $user->lang['NO_DATE']) && $mode == 'edit')
	{
		// setup the format used for the form
		$format = str_replace(array('m', 'd', 'y'), array('%m', '%d', '%Y'), strtolower($user->lang['DATE_INPUT_FORMAT']));

		// grab the event info for this topic
		$sql = 'SELECT ' .
				   'DATE_FORMAT(cal_date, "' . $format . '") as cal_date, ' .
				   'cal_interval, ' .
				   'cal_interval_units, ' .
				   'cal_repeat ' .
			   'FROM ' .
				   MYCALENDAR_TABLE . ' ' .
			   'WHERE ' .
				   "topic_id = $topic_id";
		if (!$results = $db->sql_query($sql))
		{
			trigger_error('MyCalendar_CantCheckPreviousEdit');
		}

		if ($row = $db->sql_fetchrow($results))
		{
			$cal_interval_units = $row['cal_interval_units'];
			$cal_interval = $row['cal_interval'];
			$cal_date = $row['cal_date'];
			// only if the repeat is more than 1 day (meaning it actually repeats) do we get the end date
			// else it is just a single event
			if ($row['cal_repeat'] > 1)
			{
				// have to pull a little tweak on WEEK since the interval doesn't exist in SQL
				$units = $row['cal_interval_units'] == 'WEEK' ? '* 7 DAY' : $row['cal_interval_units'];

				// we have to do another query here because SQL does not allow interval units to be a field variable
				$sql2 = 'SELECT ' .
							 'DATE_FORMAT(cal_date + INTERVAL cal_interval * (cal_repeat - 1) ' . $units . ', "' . $format . '") as cal_date_end ' .
						 'FROM ' . 
							 MYCALENDAR_TABLE . ' ' .
						 'WHERE ' .
							 'topic_id = ' . $topic_id;
				if (!$results2 = $db->sql_query($sql2))
				{
					trigger_error('MyCalendar_CantCheckPreviousEndDate');
				}

				$row2 = $db->sql_fetchrow($results2);
				$cal_date_end = $row2['cal_date_end'];
			} else if ($row['cal_repeat'] == 1)
			{
				$cal_interval = 1;
				$cal_interval_units = 'DAY';
				$cal_date_end = $user->lang['NO_DATE'];
			} else
			{
				$cal_repeat_always = 'checked="checked"';
				$cal_date_end = $user->lang['NO_DATE'];
			}
		}
	}

	$interval_units = array(
		'DAY'   => mycal_pluralize($user->lang['INTERVAL']['DAY'], $user->lang['INTERVAL']['DAYS']),
		'WEEK'  => mycal_pluralize($user->lang['INTERVAL']['WEEK'], $user->lang['INTERVAL']['WEEKS']),
		'MONTH' => mycal_pluralize($user->lang['INTERVAL']['MONTH'], $user->lang['INTERVAL']['MONTHS']),
		'YEAR'  => mycal_pluralize($user->lang['INTERVAL']['YEAR'], $user->lang['INTERVAL']['YEARS']),
	);
	$interval_unit_options = '';
	foreach($interval_units as $unit => $name)
	{
		$interval_unit_options .= '<option value="' . $unit . '"';
		if ($cal_interval_units == $unit)
		{
			$interval_unit_options .= ' selected="selected"';
		}
		$interval_unit_options .= '>' . $name . '</option>';
	}
	
	$template->assign_vars(array(
		'S_MYCALENDAR'				=> true,
		'CAL_DATE'					=> $cal_date,
		'CAL_DATE_END'				=> $cal_date_end,
		'CAL_ADVANCED_FORM'			=> ($cal_date_end != $user->lang['NO_DATE'] || $cal_repeat_always) ? '' : 'none',
		'CAL_ADVANCED_FORM_ON'		=> ($cal_date_end != $user->lang['NO_DATE'] || $cal_repeat_always) ? 'checked="checked"' : '',
		'CAL_NO_DATE'				=> $user->lang['NO_DATE'],
		'CAL_INTERVAL'				=> $cal_interval,
		'CAL_INTERVAL_UNIT_OPTIONS'	=> $interval_unit_options,
		'CAL_REPEAT_ALWAYS'			=> $cal_repeat_always,
	));
	mycal_base_calendar();
}

function mycal_base_calendar()
{
	global $user, $template, $phpbb_root_path;
	
	$user->setup('mycalendar3');

	// generate a list of months for the current language so javascript can pass it up to the calendar
	$monthList = array();
	foreach (array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December') as $month)
	{
		$monthList[] = '\'' . $user->lang['datetime'][$month] . '\'';
	}
	$monthList = implode(',', $monthList);
	
	//	Same for week-days
	$weekdays = '';
	$weekdays_long = '';
	for ($i = 0; $i < 7; $i++)
	{
		if ($weekdays != '')
		{
			$weekdays .= ', '; 
		}
		$weekdays .= '"' . $user->lang['MINICAL']['DAY']['SHORT'][$i] . '"';
		if ($weekdays_long != '')
		{
			$weekdays_long .= ', '; 
		}
		$weekdays_long .= '"' . $user->lang['MINICAL']['DAY']['LONG'][$i] . '"';
	} 
	
	$template->assign_vars(array(
		'JS_PATH'					=> $phpbb_root_path . 'js',
		'CAL_DATE_FORMAT'			=> $user->lang['DATE_INPUT_FORMAT'],
		'CAL_MONTH_LIST'			=> $monthList,
		'CAL_WEEKDAY_LIST'			=> $weekdays_long,
		'CAL_WEEKDAY_SHORT_LIST'	=> $weekdays,
	));
}

/**
 * Do a conversion from 01/01/2000 to the unix timestamp
 *
 * Convert from the date used in the form to a unix timestamp, but
 * do it based on the user preference for date formats
 *
 * @param  string date (all numbers < 10 must be '0' padded at this point)
 *
 * @access public
 * @return int unix timestamp
 */
function mycal_date2iso($in_stringDate) 
{
	global $user;
	$user->setup('mycalendar3');

	if ($in_stringDate == $user->lang['NO_DATE'])
	{
		return false;
	}

	// find the first punctuation character, which will be our delimiter
	$tmp_format = str_replace('y', 'yyyy', str_replace('m', 'mm', str_replace('d', 'dd', strtolower($user->lang['DATE_INPUT_FORMAT']))));
	$tmp_yOffset = strpos($tmp_format, 'y');
	$tmp_mOffset = strpos($tmp_format, 'm');
	$tmp_dOffset = strpos($tmp_format, 'd');

	// remap the parts to variables, at this point we assume it is coming through the wire 0 padded
	// Enforce user input checking 
	$year  = intval(substr($in_stringDate, $tmp_yOffset, 4));
	$month = intval(substr($in_stringDate, $tmp_mOffset, 2));
	$day   = intval(substr($in_stringDate, $tmp_dOffset, 2));

	if ($year < 2000 or $month < 1 or $month > 12 or $day < 1 or $day > 31)
	{
		trigger_error('MyCalendar_CantCheckDate');
	}

	return $year . '-' . $month . '-' . $day . ' 00:00:00';
}

/**
 * Determine if this post is the first post in a topic
 *
 * Simply query the topics table and determine if this post is
 * the first post in the topic...important since calendar events
 * can only be attached to the first post
 *
 * @param  int topic_id
 * @param  int post_id
 *
 * @access public
 * @return boolean is first post
 */
function mycal_first_post($topic_id, $post_id)
{
	global $db, $user;
	$user->setup('mycalendar3');

	$sql = 'SELECT ' .
			   "topic_first_post_id = $post_id as first_post " .
		   'FROM ' .
			   TOPICS_TABLE . ' as t ' .
		   'WHERE ' .
			   "topic_id = $topic_id"; 
	if (!$results = $db->sql_query($sql))
	{
		trigger_error('MyCalendar_CantCheckLeadPost');
	}

	$row = $db->sql_fetchrow($results);
	// if this is not the first post, then get out of here
	if (!$row['first_post'])
	{
		return false;
	}
	
	return true;
}

// if I were to add timezone stuff it would be here
function mycal_translate_date($in_date)
{
	global $user, $config;

	return $config['default_lang'] == 'english' ? $in_date : strtr($in_date, $user->lang['datetime']);
}

/**
 * Universal single/plural option generator
 *
 * This function will take a singular word and its plural counterpart and will
 * combine them by either appending a (s) to the singular word if the plural word
 * is formed this way, or will slash separate the singular and plural words.
 * Example: week(s), country/countries
 *
 * @param string $in_singular singular word
 * @param string $in_plural plural word
 *
 * @access public
 * @author moonbase
 * @return string combined singular/plural contruct
 */
function mycal_pluralize($in_singular, $in_plural)
{
	if (stristr($in_plural, $in_singular))
	{
		return substr($in_plural, 0, strlen($in_singular)) . ((strlen($in_plural) > strlen($in_singular)) ? '(' . substr($in_plural, strlen($in_singular)) . ')' : '');
	}
	return $in_singular . '/' . $in_plural;
}

?>