<?php
/**
*
* @package Topic Moderator MOD
* @version $Id: acp_topic_moderators.php 0.5.0 2009-06-02 11:00:00 Izya $
* @copyright (c) 2009 Izya (izya@nm.ru)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* @package acp
*/
class acp_topic_moderators
{
	var $u_action;

	function main($id, $mode)
	{
		switch ($mode)
		{
			case 'config':
				$this->tpl_name = 'acp_topic_moderators_config';
				$this->page_title = 'ACP_TOPIC_MODERATORS_CONFIG';
				$this->tm_config($id, $mode);
			break;

			case 'list':
				$this->tpl_name = 'acp_topic_moderators_list';
				$this->page_title = 'ACP_TOPIC_MODERATORS_LIST';
				$this->tm_list($id, $mode);
			break;
		}
	}

	function tm_config($id, $mode)
	{
		global $user, $template, $config;

		$user->add_lang('mods/info_mcp_topic_moderator');
		
		$submit = (isset($_POST['submit'])) ? true : false;

		if ($submit)
		{
			$config_ary = array();
			$auto_m_perms = array();
			$config_ary['tm_display_in_viewtopic'] = request_var('in_viewtopic', 0);
			$config_ary['tm_display_in_viewforum'] = request_var('in_viewforum', 0);
			$config_ary['tm_own_topic_display'] = request_var('own_topic_mdr', 0);
			$config_ary['tm_auto_m_forums'] = request_var('auto_m_forums', '');
			$config_ary['tm_auto_m_display'] = request_var('auto_m_display', 0);
			$auto_m_perms[0] = request_var('can_edit', 0);
			$auto_m_perms[1] = request_var('can_delete', 0);
			$auto_m_perms[2] = request_var('can_lock_post', 0);
			$auto_m_perms[3] = request_var('can_lock_topic', 0);
			$auto_m_perms[4] = request_var('can_reply', 0);
			$config_ary['tm_auto_m_perms'] = implode(',', $auto_m_perms);
			
			if ($config_ary['tm_auto_m_forums'] && !preg_match('/^\d+(,\d+)*$/', $config_ary['tm_auto_m_forums']))
			{
				trigger_error($user->lang['ACP_TOPIC_MDR_FORUMS_ERROR'] . adm_back_link($this->u_action));
			}
			
			if (confirm_box(true))
			{
				foreach ($config_ary as $param => $value)
				{
					if ($config[$param] != $value)
					{
						set_config($param, $value);
					}
				}
			}
			else
			{
				confirm_box(false, $user->lang['CONFIRM_OPERATION'], build_hidden_fields(array(
					'i'			=>	$id,
					'mode'		=>	$mode,
					'submit'	=>	1,
					
					'in_viewtopic'		=>	$config_ary['tm_display_in_viewtopic'],
					'in_viewforum'		=>	$config_ary['tm_display_in_viewforum'],
					'own_topic_mdr'		=>	$config_ary['tm_own_topic_display'],
					'auto_m_forums'		=>	$config_ary['tm_auto_m_forums'],
					'auto_m_display'	=>	$config_ary['tm_auto_m_display'],
					'can_edit'			=>	$auto_m_perms[0],
					'can_delete'		=>	$auto_m_perms[1],
					'can_lock_post'		=>	$auto_m_perms[2],
					'can_lock_topic'	=>	$auto_m_perms[3],
					'can_reply'			=>	$auto_m_perms[4],
				)));
			}
		}
		$ch = 'checked="checked" ';
		$perms = explode(',', $config['tm_auto_m_perms']);
		$template->assign_vars(array(
			'U_ACTION'	=>	$this->u_action,
			
			'AUTO_M_FORUMS'			=>	$config['tm_auto_m_forums'],
			'IN_VT_YES_CHECKED'		=>	$config['tm_display_in_viewtopic'] ? $ch : '',
			'IN_VT_NO_CHECKED'		=>	$config['tm_display_in_viewtopic'] ? '' : $ch,
			'IN_VF_YES_CHECKED'		=>	$config['tm_display_in_viewforum'] ? $ch : '',
			'IN_VF_NO_CHECKED'		=>	$config['tm_display_in_viewforum'] ? '' : $ch,
			'OWN_YES_CHECKED'		=>	$config['tm_own_topic_display'] ? $ch : '',
			'OWN_NO_CHECKED'		=>	$config['tm_own_topic_display'] ? '' : $ch,
			'AUTO_M_YES_CHECKED'	=>	$config['tm_auto_m_display'] ? $ch : '',
			'AUTO_M_NO_CHECKED'		=>	$config['tm_auto_m_display'] ? '' : $ch,
			'EDIT_YES_CHECKED'		=>	$perms[0] ? $ch : '',
			'EDIT_NO_CHECKED'		=>	$perms[0] ? '' : $ch,
			'DELETE_YES_CHECKED'	=>	$perms[1] ? $ch : '',
			'DELETE_NO_CHECKED'		=>	$perms[1] ? '' : $ch,
			'LOCK_POST_YES_CHECKED'	=>	$perms[2] ? $ch : '',
			'LOCK_POST_NO_CHECKED'	=>	$perms[2] ? '' : $ch,
			'LOCK_TOPIC_YES_CHECKED' =>	$perms[3] ? $ch : '',
			'LOCK_TOPIC_NO_CHECKED'	=>	$perms[3] ? '' : $ch,
			'REPLY_YES_CHECKED' 	=>	$perms[4] ? $ch : '',
			'REPLY_NO_CHECKED'		=>	$perms[4] ? '' : $ch,
		));
	}
	function tm_list($id, $mode)
	{
		global $db, $template, $phpbb_root_path, $phpEx;
		
		$per_page = 10;
		$start = request_var('start', 0);
		
		$sql = 'SELECT topic_id, topic_title, topic_moderator 
			FROM ' . TOPICS_TABLE . ' 
			WHERE topic_moderator IS NOT NULL 
				AND topic_moderator <> \'\'';
		$result = $db->sql_query($sql);
		$tm_list = array();
		while ($row = $db->sql_fetchrow($result))
		{
			$tm = unserialize($row['topic_moderator']);
			$tm_list[$tm['id']][$row['topic_id']] = $tm;
			$tm_list[$tm['id']][$row['topic_id']]['title'] = $row['topic_title'];
		}
		$db->sql_freeresult($result);
		
		if (sizeof($tm_list))
		{
			$sql = 'SELECT count(user_id) as tm_count 
				FROM ' . USERS_TABLE . ' 
				WHERE ' . $db->sql_in_set('user_id', array_keys($tm_list));
			$result = $db->sql_query($sql);
			$tm_count = $db->sql_fetchfield('tm_count');
			$db->sql_freeresult($result);
		
			$sql = 'SELECT user_id, username, user_colour 
				FROM ' . USERS_TABLE . ' 
				WHERE ' . $db->sql_in_set('user_id', array_keys($tm_list)) . ' 
				ORDER BY username';
			$result = $db->sql_query_limit($sql, $per_page, $start);
			while ($row = $db->sql_fetchrow($result))
			{
				$topics = $perms = '';
				foreach ($tm_list[$row['user_id']] as $topic_id => $topic_row)
				{
					$topics .= '> <a href="' . append_sid("{$phpbb_root_path}viewtopic.$phpEx?t=$topic_id") . '">' . $topic_row['title'] . '</a><br />';
					$perms .= '<a href="' . append_sid("{$phpbb_root_path}mcp.$phpEx?i=main&amp;mode=topic_moderator&amp;t=$topic_id") . '">' . 
						($topic_row['rep'] ? 'r' : '-') . 
						($topic_row['edit'] ? 'e' : '-') . 
						($topic_row['del'] ? 'd' : '-') . 
						($topic_row['l_p'] ? 'p' : '-') . 
						($topic_row['l_t'] ? 't' : '-') . '</a><br />';
				}
				$template->assign_block_vars('tmrow', array(
					'USERNAME'	=>	get_username_string('full', $row['user_id'], $row['username'], $row['user_colour']),
					'TOPICS'	=>	$topics,
					'PERMS'		=>	$perms
				));
			}
			$db->sql_freeresult($result);
		}
		
		$template->assign_vars(array(
			'S_ON_PAGE'		=> on_page($tm_count, $per_page, $start),
			'PAGINATION'	=> generate_pagination($this->u_action, $tm_count, $per_page, $start, true),
		));
	}
}

?>