<?php
/**
*
* @package Topic Moderator MOD
* @version $Id: acp_topic_moderators.php 0.5.0 2009-06-02 11:00:00 Izya $
* @copyright (c) 2009 Izya (izya@nm.ru)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @package module_install
*/
class acp_topic_moderators_info
{
	function module()
	{
		return array(
			'filename'	=> 'acp_topic_moderators',
			'title'		=> 'ACP_TOPIC_MODERATORS',
			'version'	=> '0.5.0',
			'modes'		=> array(
				'config'	=> array('title' => 'ACP_TOPIC_MODERATORS_CONFIG', 'auth' => 'acl_a_topicmdr', 'cat' => array('ACP_TOPIC_MODERATORS')),
				'list'		=> array('title' => 'ACP_TOPIC_MODERATORS_LIST', 'auth' => 'acl_a_topicmdr', 'cat' => array('ACP_TOPIC_MODERATORS')),
			),
		);
	}

	function install()
	{
	}

	function uninstall()
	{
	}
}

?>