<?php
/**
* 
* @package Topic Moderator MOD
* @version $Id: functions_topic_moderator.php 0.5.0 2009-06-02 11:00:00 Izya $
* @copyright (c) 2009 Izya (izya@nm.ru)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* Get topic moderator info
* @return	array
*/
function get_topic_moderator($topic_id)
{
	global $db;
	
	$sql = 'SELECT topic_moderator, topic_title 
		FROM ' . TOPICS_TABLE . ' 
		WHERE topic_id = ' . (int)$topic_id;
	$result = $db->sql_query($sql);
	$row = $db->sql_fetchrow($result);
	$db->sql_freeresult($result);
	if (!$row)
	{
		trigger_error('NO_TOPIC');
	}
	if (!$row['topic_moderator'])
	{
		$tm_info = array('topic_title' => $row['topic_title']);
		return $tm_info;
	}	
	$tm_info = unserialize($row['topic_moderator']);
	$tm_info['topic_title'] = $row['topic_title'];
	
	$sql = 'SELECT username, user_colour 
		FROM ' . USERS_TABLE . ' 
		WHERE user_id = ' . (int)$tm_info['id'];
	$result = $db->sql_query($sql);
	$row = $db->sql_fetchrow($result);
	$db->sql_freeresult($result);
	if (!$row)
	{
		$tm_info = array('topic_title' => $tm_info['topic_title']);
		return $tm_info;
	}
	$tm_info['name'] = $row['username'];
	$tm_info['colour'] = $row['user_colour'];
	
	return $tm_info;
}

/**
* Save updated topic moderator info into DB
* $param	array	$ary_to_store	array('id' => (int) user_id, 'edit' => (bool) can edit,
*									'del' => (bool) can delete, 'l_p' => (bool) can lock post,
*									'l_t' => (bool) can lock topic, 'rep' => (bool) can reply)
*									Must be not array for removing topic moderator
* no return
*/
function set_topic_moderator($topic_id, $ary_to_store)
{
	global $db;
	
	$value = is_array($ary_to_store) ? $db->sql_escape(serialize($ary_to_store)) : '';
	
	$sql = 'UPDATE ' . TOPICS_TABLE . ' 
		SET topic_moderator = \'' . $value . '\' 
		WHERE topic_id = ' . (int)$topic_id;
	$db->sql_query($sql);
}

/**
* Handling mcp actions on topic moderator
*/
function mcp_topic_moderator($id, $mode, $action)
{
	global $template, $db, $user;
	global $phpEx, $phpbb_root_path;
	
	$url = append_sid("{$phpbb_root_path}mcp.$phpEx?" . extra_url());
	
	$forum_id = request_var('f', 0);
	$topic_id = request_var('t', 0);
	
	$tm_info = get_topic_moderator($topic_id);
	
	if ($action == 'set')
	{
		$can_edit = request_var('can_edit', 0);
		$can_delete = request_var('can_delete', 0);
		$can_lock_post = request_var('can_lock_post', 0);
		$can_lock_topic = request_var('can_lock_topic', 0);
		$can_reply = request_var('can_reply', 0);
		$tm_username = request_var('username', '', true);
		
		if (confirm_box(true))
		{
			$tm_username_clean = utf8_clean_string($tm_username);
		
			$sql = 'SELECT user_id, username, user_colour 
				FROM ' . USERS_TABLE . ' 
				WHERE username_clean = \'' . $db->sql_escape($tm_username_clean) . '\'';
			$result = $db->sql_query($sql);
			$row = $db->sql_fetchrow($result);
			$db->sql_freeresult($result);
			if (!$row)
			{
				trigger_error('NO_USER');
			}
			$tm_user_id = $row['user_id'];
		
			if (sizeof($tm_info) == 1 || $tm_user_id != $tm_info['id'] || $can_edit != $tm_info['edit'] || $can_delete != $tm_info['del'] || $can_lock_post != $tm_info['l_p'] || $can_lock_topic != $tm_info['l_t'] || $can_reply != $tm_info['rep'])
			{
				$ary_to_store = array(
					'id'	=>	$tm_user_id,
					'edit'	=>	$can_edit,
					'del'	=>	$can_delete,
					'l_p'	=>	$can_lock_post,
					'l_t'	=>	$can_lock_topic,
					'rep'	=>	$can_reply,
				);
				set_topic_moderator($topic_id, $ary_to_store);
				
				if (sizeof($tm_info) == 1)
				{
					$m_action = 'SET';
					$log_user = get_username_string('full', $tm_user_id, $row['username'], $row['user_colour']);
				}
				else if ($tm_user_id != $tm_info['id'])
				{
					$m_action = 'CHANGE';
					$log_user = get_username_string('full', $tm_info['id'], $tm_info['name'], $tm_info['colour']) . ' -> ' . get_username_string('full', $tm_user_id, $row['username'], $row['user_colour']);
				}
				else
				{
					$m_action = 'CHPERMS';
					$log_user = get_username_string('full', $tm_info['id'], $tm_info['name'], $tm_info['colour']);
				}
				
				add_log('mod', $forum_id, $topic_id, 'LOG_MOD_' . $m_action . '_TOPIC_MODERATOR', $tm_info['topic_title'], $log_user);
				$tm_info = array_merge($tm_info, $ary_to_store, array(
					'name'		=>	$row['username'],
					'colour'	=>	$row['user_colour']
				));
			}
		}
		else if ($tm_username)
		{
			$hidden_fields = build_hidden_fields(array(
				'action'			=>	'set',
				'username'			=>	$tm_username,
				'can_edit'			=>	$can_edit,
				'can_delete'		=>	$can_delete,
				'can_lock_post'		=>	$can_lock_post,
				'can_lock_topic'	=>	$can_lock_topic,
				'can_reply'			=>	$can_reply,
			));
			confirm_box(false, $user->lang['CONFIRM_OPERATION'], $hidden_fields);
		}
	}
	else if ($action == 'remove' && isset($tm_info['id']))
	{
		if (confirm_box(true))
		{
			set_topic_moderator($topic_id, false);
			add_log('mod', $forum_id, $topic_id, 'LOG_MOD_REMOVE_TOPIC_MODERATOR', $tm_info['topic_title'], get_username_string('full', $tm_info['id'], $tm_info['name'], $tm_info['colour']));
			$tm_info = array('topic_title' => $tm_info['topic_title']);
		}
		else
		{
			confirm_box(false, $user->lang['CONFIRM_OPERATION']);
		}
	}
	
	$template->assign_vars(array(
		'TOPIC_TITLE'		=>	$tm_info['topic_title'],
		'TOPIC_MODERATOR'	=>	!isset($tm_info['id']) ? false : get_username_string('full', $tm_info['id'], $tm_info['name'], $tm_info['colour']),
		'TM_USERNAME'		=>	isset($tm_info['name']) ? $tm_info['name'] : '',
		
		'U_VIEW_TOPIC'		=>	append_sid("{$phpbb_root_path}viewtopic.$phpEx", 'f=' . $forum_id . '&amp;t=' . $topic_id),
		'U_REMOVE_TM'		=>	"$url&amp;i=$id&amp;mode=$mode&amp;action=remove",
		'U_FIND_USERNAME'	=>	append_sid("{$phpbb_root_path}memberlist.$phpEx", 'mode=searchuser&amp;form=mcp&amp;field=username&amp;select_single=true'),
		
		'S_MCP_ACTION'		=>	"$url&amp;i=$id&amp;mode=$mode",
		'S_HIDDEN_FIELDS'	=>	build_hidden_fields(array('action' => 'set')),
		
		'CAN_EDIT'			=>	isset($tm_info['edit']) ? $tm_info['edit'] : 0,
		'CAN_DELETE'		=>	isset($tm_info['del']) ? $tm_info['del'] : 0,
		'CAN_LOCK_POST'		=>	isset($tm_info['l_p']) ? $tm_info['l_p'] : 0,
		'CAN_LOCK_TOPIC'	=>	isset($tm_info['l_t']) ? $tm_info['l_t'] : 0,
		'CAN_REPLY'			=>	isset($tm_info['rep']) ? $tm_info['rep'] : 0,
	));
}

?>