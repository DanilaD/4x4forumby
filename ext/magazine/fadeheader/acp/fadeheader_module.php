<?php
/**
*
* @package phpBB Extension - FadeHeader
* @copyright (c) 2015 Ammin - http://www.forum.magazine.edu.gr/
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace magazine\fadeheader\acp;

class fadeheader_module
{
var $u_action;

	function main($id, $mode)
	{
		global $db, $user, $auth, $template, $cache, $request;
		global $config, $phpbb_root_path, $phpbb_admin_path, $phpEx;

		$this->tpl_name = 'acp_fadeheader_config';
		$this->page_title = $user->lang['ACP_FADEHEADER_CONFIG_SETTINGS'];				
		
		add_form_key('acp_fadeheader_config');

		$submit = $request->is_set_post('submit');
		if ($submit)
		{
			if (!check_form_key('acp_fadeheader_config'))
			{
				trigger_error('FORM_INVALID');
			}
			$config->set('fadeheader_enablepm', $request->variable('fadeheader_enablepm', 0));
			$config->set('fadeheader_enablereg', $request->variable('fadeheader_enablereg', 0));
			$config->set('fadeheader_enablebh', $request->variable('fadeheader_enablebh', 0));			
			$config->set('fadeheader_enablebf', $request->variable('fadeheader_enablebf', 0));

			$config->set('fadeheader_image', $request->variable('fadeheader_image', ''));
			$config->set('fadeheader_image2', $request->variable('fadeheader_image2', ''));	
			$config->set('fadeheader_image3', $request->variable('fadeheader_image3', ''));			
			$config->set('fadeheader_temp', $request->variable('fadeheader_temp', ''));			
			$config->set('fadeheader_height', $request->variable('fadeheader_height', ''));		
			$config->set('fadeheader_text_twitter', $request->variable('fadeheader_text_twitter', ''));			
			$config->set('fadeheader_text_field', $request->variable('fadeheader_text_field', ''));								
			
			trigger_error($user->lang['FADEHEADER_CONFIG_SAVED'] . adm_back_link($this->u_action));
		}

		$template->assign_vars(array(
			'FADEHEADER_VERSION'		=> (isset($config['fadeheader_version'])) ? $config['fadeheader_version'] : '',
			'FADEHEADER_ENABLEPM'		=> (!empty($config['fadeheader_enablepm'])) ? true : false,
			'FADEHEADER_ENABLEREG'		=> (!empty($config['fadeheader_enablereg'])) ? true : false,
			'FADEHEADER_ENABLEBH'		=> (!empty($config['fadeheader_enablebh'])) ? true : false,			
			'FADEHEADER_ENABLEBF'		=> (!empty($config['fadeheader_enablebf'])) ? true : false,								

			'FADEHEADER_IMAGE'     	    => (isset($config['fadeheader_image'])) ? $config['fadeheader_image'] : '',	
			'FADEHEADER_IMAGE2'     	=> (isset($config['fadeheader_image2'])) ? $config['fadeheader_image2'] : '',
			'FADEHEADER_IMAGE3'     	=> (isset($config['fadeheader_image3'])) ? $config['fadeheader_image3'] : '',			
			'FADEHEADER_TEMP'     	    => (isset($config['fadeheader_temp'])) ? $config['fadeheader_temp'] : '',			
			'FADEHEADER_HEIGHT'     	=> (isset($config['fadeheader_height'])) ? $config['fadeheader_height'] : '',
			'FADEHEADER_TEXT_TWITTER'	=> (isset($config['fadeheader_text_twitter'])) ? $config['fadeheader_text_twitter'] : '',			
			'FADEHEADER_TEXT_FIELD'	    => (isset($config['fadeheader_text_field'])) ? $config['fadeheader_text_field'] : '',
			'U_ACTION'					=> $this->u_action,
		));
	}
}
