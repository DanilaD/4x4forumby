<?php
/**
*
* @package phpBB Extension - FadeHeader
* @copyright (c) 2015 Ammin - http://www.forum.magazine.edu.gr/
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace magazine\fadeheader\acp;

class fadeheader_info
 {
	function module()
	{
		 return array(
			'filename'	=> '\magazine\fadeheader\acp\sharethis_module',
			'title'		=> 'ACP_FADEHEADER',
			'modes'		=> array(
			'config'	=> array('title' => 'ACP_FADEHEADER_CONFIG_SETTINGS', 'auth' => 'ext_magazine/fadeheader && acl_a_board', 'cat' => array('ACP_FADEHEADER')),
			),
		);
	}
}
