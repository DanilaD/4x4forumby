<?php
/**
*
* @package phpBB Extension - FadeHeader
* @copyright (c) 2015 Ammin - http://www.forum.magazine.edu.gr/
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace magazine\fadeheader\migrations;

class fadeheader_schema extends \phpbb\db\migration\migration
{

	public function update_data()
	{
		return array(
			// Add configs
			array('config.add', array('fadeheader_enablepm', '')),
			array('config.add', array('fadeheader_enablereg', '')),
			array('config.add', array('fadeheader_image', '')),	
			array('config.add', array('fadeheader_image2', '')),			
			array('config.add', array('fadeheader_image3', '')),			
			array('config.add', array('fadeheader_temp', '')),			
			array('config.add', array('fadeheader_height', '')),			
			array('config.add', array('fadeheader_enable_text_twitter', '')),			
			array('config.add', array('fadeheader_enable_text_field', '')),
			array('config.add', array('fadeheader_version', '1.1.4')),				
			array('config.add', array('fadeheader_enablebh', '')),			
			array('config.add', array('fadeheader_enablebf', '')),			
		);
	}

	public function revert_data()
	{
		return array(
			array('config.remove', array('fadeheader_enablepm')),
			array('config.remove', array('fadeheader_enablereg')),
			array('config.remove', array('fadeheader_image')),
			array('config.remove', array('fadeheader_image2')),
			array('config.remove', array('fadeheader_image3')),			
			array('config.remove', array('fadeheader_temp')),			
			array('config.remove', array('fadeheader_height')),
			array('config.remove', array('fadeheader_enable_text_twitter')),			
			array('config.remove', array('fadeheader_enable_text_field')),
			array('config.remove', array('fadeheader_version')),
			
			array('config.remove', array('fadeheader_enablebh')),			
			array('config.remove', array('fadeheader_enablebf')),			
		);
	}
}
