<?php
/**
*
* @package phpBB Extension - FadeHeader
* @copyright (c) 2015 Ammin - http://www.forum.magazine.edu.gr/
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace magazine\fadeheader\migrations;

class fadeheader_module extends \phpbb\db\migration\migration
{

	public function update_data()
	{
		return array(
			array('module.add', array('acp', 'ACP_CAT_DOT_MODS', 'ACP_FADEHEADER')),
			array('module.add', array(
			'acp', 'ACP_FADEHEADER', array(
			'module_basename'	=> '\magazine\fadeheader\acp\fadeheader_module', 'modes'		=> array('config'),
				),
			)),
		);
	}
}