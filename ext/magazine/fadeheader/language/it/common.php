<?php
/**
*
* @package phpBB Extension - FadeHeader
* @copyright (c) 2015 Ammin - http://www.forum.magazine.edu.gr/
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'FADEHEADER_VERSION'			 => 'Versione',
	'FADEHEADER_ENABLEPM'			 => 'Abilita Fade Header',	
	'FADEHEADER_ENABLEPM_EXPLAIN'	 => 'Abilita o disabilita estensione',
	'FADEHEADER_TEMP'	             => 'Secondi',	
	'FADEHEADER_TEMP_EXPLAIN'	     => 'Inserisci un numero espresso in secondi per il cambio immagini. Consigliato: 10',
	'FADEHEADER_HEIGHT'	             => 'Altezza',	
	'FADEHEADER_HEIGHT_EXPLAIN'	     => 'Inserisci altezza immagini in pixel. Consigliato: 180',
	'FADEHEADER_TEXT_TWITTER'		 => 'Immagine di testata',
	'FADEHEADER_TEXT_TWITTER_EXPLAIN'=> 'Inserisci URL intero della prima immagine di testata.',
	'FADEHEADER_IMAGE'		         => 'Immagine di coda',
	'FADEHEADER_IMAGE_EXPLAIN'		 => 'Inserisci URL intero della immagine di coda.',
	
	'FADEHEADER_ENABLEREG'			=> 'Abilita seconda immagine',
	'FADEHEADER_ENABLEREG_EXPLAIN'	=> 'Abilita o disabilita seconda immagine',
	'FADEHEADER_IMAGE2'			    => 'Url seconda immagine',	
	'FADEHEADER_IMAGE2_EXPLAIN'	    => 'Inserisci intero URL della seconda immagine',

	'FADEHEADER_ENABLEBH'			=> 'Abilita terza immagine',
	'FADEHEADER_ENABLEBH_EXPLAIN'	=> 'Abilita o disabilita la terza immagine',
	'FADEHEADER_IMAGE3'			    => 'Url terza immagine',	
	'FADEHEADER_IMAGE3_EXPLAIN'	    => 'Inserisci intero URL della terza immagine',

	'FADEHEADER_ENABLEBF'			=> 'Abilita quarta immagine',
	'FADEHEADER_ENABLEBF_EXPLAIN'	=> 'Abilita o disabilita la quarta immagine',	
	'FADEHEADER_TEXT_FIELD'			=> 'Url quarta immagine',
	'FADEHEADER_TEXT_FIELD_EXPLAIN'	=> 'Inserisci intero URL della quarta immagine',
	
	'FADEHEADER_CONFIG_SAVED'		=> 'Opzioni salvate',	
	'COPYRIGHT_FADEHEADER'		    => 'Fade Header by <a href="http://www.techuniverse.it/forum/">Forum Tech Universe</a>',

));
