<?php
/**
*
* @package phpBB Extension - ShareThis
* @copyright (c) 2015 Ammin - http://www.forum.magazine.edu.gr/
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_FADEHEADER'					  => 'Fade Header',
	'ACP_FADEHEADER_CONFIG_SETTINGS'	  => 'options extension',	
	'ACP_FADEHEADER_CONFIG_SET'			  => 'Options',	
	'ACP_FADEHEADER_CONFIG_STYLE'		  => 'Additional Images',
	'ACP_FADEHEADER_CONFIG_STYLE_EXPLAIN' => 'Enable or disable the additional images',	
));

