<?php
/**
*
* @package phpBB Extension - FadeHeader
* @copyright (c) 2015 Ammin - http://www.forum.magazine.edu.gr/
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'FADEHEADER_VERSION'			 => 'Version',
	'FADEHEADER_ENABLEPM'			 => 'Enable Fade Header',	
	'FADEHEADER_ENABLEPM_EXPLAIN'	 => 'Enable or disable extension',
	'FADEHEADER_TEMP'	             => 'Seconds',	
	'FADEHEADER_TEMP_EXPLAIN'	     => 'Enter the number of seconds to change the images. Recommended: 10',
	'FADEHEADER_HEIGHT'	             => 'Height',	
	'FADEHEADER_HEIGHT_EXPLAIN'	     => 'Enter the height of the image in pixels. Recommended: 180',
	'FADEHEADER_TEXT_TWITTER'		 => 'Heading image',
	'FADEHEADER_TEXT_TWITTER_EXPLAIN'=> 'Enter the entire link of the first image of the head.',
	'FADEHEADER_IMAGE'		         => 'Last image',
	'FADEHEADER_IMAGE_EXPLAIN'		 => 'Enter the entire link of the last image of the tail.',
	
	'FADEHEADER_ENABLEREG'			=> 'Enable second image',
	'FADEHEADER_ENABLEREG_EXPLAIN'	=> 'Enable or disable the second image',
	'FADEHEADER_IMAGE2'			    => 'Url second image',	
	'FADEHEADER_IMAGE2_EXPLAIN'	    => 'Enter the full URL of the image that will appear for the second',

	'FADEHEADER_ENABLEBH'			=> 'Enable third image',
	'FADEHEADER_ENABLEBH_EXPLAIN'	=> 'Enable or disable the third image',
	'FADEHEADER_IMAGE3'			    => 'Url third image',	
	'FADEHEADER_IMAGE3_EXPLAIN'	    => 'Enter the full URL of the image that will appear for the third',

	'FADEHEADER_ENABLEBF'			=> 'Enable fourth image',
	'FADEHEADER_ENABLEBF_EXPLAIN'	=> 'Enable or disable the fourth image',	
	'FADEHEADER_TEXT_FIELD'			=> 'Url fourth image',
	'FADEHEADER_TEXT_FIELD_EXPLAIN'	=> 'Enter the full URL of the image that will appear for the fourth',
	
	'FADEHEADER_CONFIG_SAVED'		=> 'Options saved',	
	'COPYRIGHT_FADEHEADER'		    => 'Fade Header by <strong>www.techuniverse.it/forum/</strong>',

));
