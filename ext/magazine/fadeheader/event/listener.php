<?php
/**
*
* @package phpBB Extension - Fade Header
* @copyright (c) 2015 Ammin - http://www.forum.magazine.edu.gr/
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace magazine\fadeheader\event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class listener implements EventSubscriberInterface
{

	protected $helper;

	protected $template;

	protected $config;

	public function __construct(\phpbb\controller\helper $helper, \phpbb\template\template $template, \phpbb\config\config $config)
	{
		$this->helper = $helper;
		$this->template = $template;
		$this->config = $config;
	}

	static public function getSubscribedEvents()
	{
		return array(
			'core.user_setup' => 'load_language_on_setup',
			'core.page_header'	=> 'add_page_header_link',
		);
	}

	public function load_language_on_setup($event)
	{
		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = array(
			'ext_name' => 'magazine/fadeheader',
			'lang_set' => 'common',
		);
		$event['lang_set_ext'] = $lang_set_ext;
	}

	public function add_page_header_link($event)
	{
		$this->template->assign_vars(array(
			'FADEHEADER_ENABLEPM'	 => $this->config['fadeheader_enablepm'] ? true : false,
			'FADEHEADER_ENABLEREG'	 => $this->config['fadeheader_enablereg'] ? true : false,	
			'FADEHEADER_ENABLEBH'	 => $this->config['fadeheader_enablebh'] ? true : false,			
			'FADEHEADER_ENABLEBF'	 => $this->config['fadeheader_enablebf'] ? true : false,							

			'FADEHEADER_IMAGE'       => (isset($this->config['fadeheader_image'])) ? $this->config['fadeheader_image'] : '',
			'FADEHEADER_IMAGE2'      => (isset($this->config['fadeheader_image2'])) ? $this->config['fadeheader_image2'] : '',
			'FADEHEADER_IMAGE3'      => (isset($this->config['fadeheader_image3'])) ? $this->config['fadeheader_image3'] : '',		
			'FADEHEADER_TEMP'        => (isset($this->config['fadeheader_temp'])) ? $this->config['fadeheader_temp'] : '',			
			'FADEHEADER_HEIGHT'      => (isset($this->config['fadeheader_height'])) ? $this->config['fadeheader_height'] : '',
			'FADEHEADER_TEXT_TWITTER'=> (isset($this->config['fadeheader_text_twitter'])) ? $this->config['fadeheader_text_twitter'] : '',			
			'FADEHEADER_TEXT_FIELD'	 => (isset($this->config['fadeheader_text_field'])) ? $this->config['fadeheader_text_field'] : '',
		));
	}
}