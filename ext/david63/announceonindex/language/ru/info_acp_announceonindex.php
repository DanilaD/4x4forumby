<?php
/**
*
* @package Announcements on index
* @copyright (c) 2015 david63
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	'ALLOW_EVENTS'					=> 'Разрешить шаблоны событий',
	'ALLOW_EVENTS_EXPLAIN'			=> 'Разрешить пользователям использовать шаблоны событий в объявлениях.<br />Выключите это, если возникнут проблемы с отображением событий.',
	'ALLOW_GUESTS'					=> 'Показывать глобальные объявления',
	'ALLOW_GUESTS_EXPLAIN'			=> 'Показывать глобальные объявления.',

	'ANNOUNCE_ON_INDEX'				=> 'Объявления на главной',
	'ANNOUNCE_ON_INDEX_ENABLE'		=> 'Включить объявления',
	'ANNOUNCE_ON_INDEX_EXPLAIN' 	=> 'Управление настройками объявлений.',
	'ANNOUNCE_ON_INDEX_LOG'			=> '<strong>Настройки объявлений на главной обновлены </strong>',
	'ANNOUNCE_ON_INDEX_MANAGE'		=> 'Управление объявлениями',
	'ANNOUNCE_ON_INDEX_OPTIONS'		=> 'Опции объявлений',

	'SHOW_ANNOUNCEMENTS'			=> 'Показывать объявления',
	'SHOW_ANNOUNCEMENTS_EXPLAIN'	=> 'Показывать все объявления на главной странице.',
	'SHOW_GLOBALS'					=> 'Показывать глобальные объявления',
	'SHOW_GLOBALS_EXPLAIN'			=> 'Показывать глобальные объявления на главной странице.',
));
