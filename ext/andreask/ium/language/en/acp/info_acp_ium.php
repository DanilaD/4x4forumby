<?php

/**
* This file is part of the phpBB Forum extension package
* IUM (Inactive User Manager).
*
* @copyright (c) 2016 by Andreas Kourtidis
* @license   GNU General Public License, version 2 (GPL-2.0)
*
* For full copyright and license information, please see
* the CREDITS.txt file.
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty( $lang) || !is_array($lang) )
{
	$lang = array();
}

$lang = array_merge(
		$lang, array(
	//
	'ACP_IUM'	=>	'Настройки',
	'ACP_IUM_LIST'	=>	'Статистика пользователей',
	'ACP_IUM_TITLE'	=>	'E-mail уведомления',
	'ACP_IUM_TITLE2'	=> 'Список неактивных пользователей',
	'ACP_IUM_APPROVAL_LIST'	=> 'Игноировать/удалить пользователей',
	// ACP configuration page
	'ANDREASK_IUM_ENABLE'	=>	'Enable Advanced Inactive User Reminder ',
	'ANDREASK_IUM_INTERVAL'	=>	'Интервал ',
	'ANDREASK_IUM_EMAIL_LIMIT'	=>	'Лимит E-mails ',
	'ANDREASK_IUM_TOP_USER_THREADS'	=>	'Include user\'s top topics ',
	'ANDREASK_IUM_TOP_USER_THREADS_COUNT'	=>	'How many topics ',
	'ANDREASK_IUM_TOP_FORUM_THREADS'	=>	'Include board\'s top topics ',
	'ANDREASK_IUM_TOP_FORUM_THREADS_COUNT'	=>	'How many topics ',
	'ANDREASK_IUM_SELF_DELETE'	=>	'User is able to self delete',
	'ANDREASK_IUM_DELETE_APPROVE'			=>	'Require approval for self delete requests',
	'ANDREASK_IUM_KEEP_POSTS'				=>	'Keep posts of deleted users',
	'ANDREASK_IUM_AUTO_DEL'					=>	'Auto delete User',
	'ANDREASK_IUM_AUTO_DEL_DAYS'			=>	'Days after',
	'ANDREASK_IUM_TEST_EMAIL'				=>	'Sent test e-mail',
	'ANDREASK_IUM_INCLUDED_FORUMS'			=>	'Included forums',
	'ANDREASK_IUM_EXCLUDE_FORUM'			=>	'Exclude',

	'ANDREASK_IUM_EXCLUDED_FORUMS'			=>	'Excluded forums',
	'ANDREASK_IUM_INCLUDE_FORUM'			=>	'Include',
	'SELECT_A_FORUM'						=>	'Please select a forum',
	'EXCLUDED_EMPTY'						=>	'No excluded forums...',


	// ACP configuration page Explanations
	'ANDREASK_IUM_ENABLE_EXPLAIN'	=>	'If enabled, the extension will start sending reminders to "sleepers".',
	'ANDREASK_IUM_INTERVAL_EXPLAIN'	=>	'It is the interval of days to count back to consider a user a "sleeper". Recommended is 30 days',
	'ANDREASK_IUM_EMAIL_LIMIT_EXPLAIN'	=>	'Amount of the reminders that can be sent <b>per day</b>. Recommended is ~250. But do check with your provider for any limit of e-mails per day',
	'ANDREASK_IUM_TOP_USER_THREADS_EXPLAIN'	=>	'If enabled, mail will include user\'s top active topics since user\'s last visit.',
	'ANDREASK_IUM_TOP_USER_THREADS_COUNT_EXPLAIN'	=>	'Amount of user\'s top topics that should be included in the e-mail.',
	'ANDREASK_IUM_TOP_FORUM_THREADS_EXPLAIN'	=>	'If enabled, mail will include board\'s top topics since user\'s last visit.',
	'ANDREASK_IUM_TOP_FORUM_THREADS_COUNT_EXPLAIN'	=>	'Amount of forum topics that should be included in e-mail',
	'ANDREASK_IUM_SELF_DELETE_EXPLAIN'	=>	'If enabled, a link to a page "<strong>board_url/ium/{random_key}</strong>" will be included to user and they will be able to self delete their acount..',
	'ANDREASK_IUM_DELETE_APPROVE_EXPLAIN'	=>	'If enabled all self delete request will have to be approved by the administrator.',
	'ANDREASK_IUM_KEEP_POSTS_EXPLAIN'	=>	'"Yes" will delete user but will <strong>keep</strong> the post, "No" will delete the posts of the user as well.',
	'ANDREASK_IUM_IGNORE_LIST_EXPLAIN'	=>	'Here you can manage the users that you want to ignore (don\'t send reminder) or remove them from the ignore list.<br/><strong>Each user in a new line.</strong><br/>Note, the following groups are <strong>ignored by default</strong> : 1. GUESTS, 4. GLOBAL_MODERATORS, 5. ADMINISTRATOR and 6. BOTS',
	'ANDREASK_IUM_AUTO_DEL_EXPLAIN'			=>	'Users will be autodeleted after a given amount of days if they do not return after the 3 reminders.',
	'ANDREASK_IUM_AUTO_DEL_DAYS_EXPLAIN'	=>	'Ammount of days to wait until auto delete a user after the requested day.',
	'ANDREASK_IUM_TEST_EMAIL_EXPLAIN'		=>	'A test e-mail will be sent to "%s"',
	'ANDREASK_IUM_INCLUDED_FORUMS_EXPLAIN'	=>	'Select a category or subcategory to <strong>exclude</strong> it from the top topics lists that are sent to the users',
	'ANDREASK_IUM_EXCLUDED_FORUMS_EXPLAIN'	=>	'Select a category or subcategory to <strong>include</strong> it from the top topics lists that are sent to the users',

	// configuration page Legend
	'IUM_INACTIVE_USERS_EXPLAIN'	=>	'Список пользователей, которые давно не пользоватлись конференцией.',
	'ACP_IUM_SETTINGS'	=>	'Inactive User Reminder Settings',
	'ACP_IUM_MAIL_SETTINGS'	=>	'Reminder Settings',
	'ACP_IUM_MAIL_INCLUDE_SETTINGS'	=>	'Reminder Include Settings',
	'ACP_IUM_DANGER'	=>	'Danger Area',
	// configuration page
	'INACTIVE_MAIL_SENT_TO'			=>	'A sample of email for inactive users was sent to "%s"',
	'SLEEPER_MAIL_SENT_TO'			=>	'A sample of email for inactive users was sent to "%s"',
	'SEND_SLEEPER'					=>	'Отправить шаблон напоминание о нас',
	'SEND_INACTIVE'					=>	'Отправить шаблон неактивизированного пользователя',
	'PLUS_SUBFORUMS'				=>	'+Подфорумы',
	// Sort by, options for the inactive users list
	'ACP_IUM_INACTIVE'	=> array(	0	=>	'Активный',
									1	=>	'Registration pre-activation',
									2	=>	'Профиль изменен',
									3	=>	'Деактивирован администратором',
									4	=>	'Забанен',
									5	=>	'Временно забанен'),
	'NEVER_CONNECTED'	=>	'Никогда не входил',
	// Inactive users list page
	'ACP_IUM_NODATE'	=>	'Пользователь <strong>активен</strong>',
	'ACP_USERS_WITH_POSTS'	=>	'Пользователи с сообщениями',
	'LAST_SENT_REMINDER'	=>	'Предыдущее напоминание',
	'NO_REMINDER_COUNT'	=>	'Пока ещё нет напоминаний',
	'COUNT'	=>	'Кол-во напоминаний',
	'NO_PREVIOUS_SENT_DATE'	=> 'Ещё не отправлено напоминаний',
	'REMINDER_DATE'	=>	'Последнее напоминание',
	'NO_REMINDER_SENT_YET'	=>	'Пока не отправлено ни одного сообщения',
	'IUM_INACTIVE_REASON'	=>	'Статус',
	// Delete approval page
	'ACP_IUM_APPROVAL_LIST_TITLE'	=> 'Deletion approval list',
	'APPROVAL_LIST_PAGE_TITLE'	=> 'Deletion approval list',
	'IUM_APPROVAL_LIST_EXPLAIN'	=> 'Approval list of users with a request for deleting their account',
	'NO_REQUESTS'			=> 'No requests yet',
	'NO_USER_SELECTED'		=>	'No user selected.',
	'IUM_MANAGMENT'			=>	'Inactive User Managment',
	'IGNORE_USER_LIST'		=>	'Add users to ignore list',
	'IGNORED_USERS_LIST'	=>	'List of users that are ignored',
	'ADD_IGNORE_USER'		=>	'Добавить в список',
	'REMOVE_IGNORE_USER'	=>	'Удалить из списка',
	'DELETED_SUCCESSFULLY'	=>	'Deleted successfully.',
	'REQUEST_TYPE'			=>	'Тип запроса',
	'APPROVE'				=>	'Одобрено',
	'NO_USER_TYPED'			=>	'Не выбран пользователь',
	// Sort Lists
	'COUNT_BACK'	=>	'Интервал',
	'ACP_DESCENDING'	=>	'В порядке убывания',
	'SORT_BY_SELECT'	=>	'Сортировка',
	'REQUEST_DATE'		=>	'Дата запроса на удаление',
	'SELECT'	=>	'Дата D/M/Y',
	'THIRTY_DAYS'	=>	'30 дней',
	'SIXTY_DAYS'	=>	'60 дней',
	'NINETY_DAYS'	=>	'90 дней',
	'FOUR_MONTHS'	=>	'4 месяца',
	'SIX_MONTHS'	=>	'6 месяцев',
	'NINE_MONTHS'	=>	'9 месяцев',
	'ONE_YEAR'		=>	'1 год',
	'TWO_YEARS'		=>	'2 года',
	'THREE_YEARS'	=>	'3 года',
	'FIVE_YEARS'	=>	'5 лет',
	'SEVEN_YEARS'	=>	'7 лет',
	'DECADE'		=>	'1 декада',

	// Log
	'SENT_REMINDER_TO_ADMIN'	=>	'Шаблон "%1$s" отправлен "%2$s"',
	'SENT_REMINDERS'			=>	'%s отправленных напоминаний.',
	'USERS_DELETED'				=>	'"%1$s" удаленные пользователи, тип запроса : "%2$s"',
	'USER_DELETED'				=>	'Пользователь "%1$s" удален, тип запроса : "%2$s"',
	'SOMETHING_WRONG'			=>	'Что-то пошло не так. Указанные пользователи для удаления не отмечены в базе данных',
	'USER_SELF_DELETED'			=>	'Пользователь самоудалился. Конфигурация сообщений отправлена "%s"',


	)
);
