<?php
/**
*
* @package National Flags
* @copyright (c) 2015 Rich McGirr(RMcGirr83)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

namespace rmcgirr83\nationalflags\core;

class flag_position_constants
{
	const AFTER_CONTACT_FIELDS = 0;
	const AFTER_AVATAR = 1;
	const BEFORE_AVATAR = 2;
	const AFTER_USER_NAME = 3;
	const BEFORE_USER_NAME = 4;
	const ABOVE_RANK = 5;
	const BELOW_RANK = 6;
	const AFTER_PROFILE_FIELDS = 7;
	const BEFORE_PROFILE_FIELDS = 8;
}
