<?php
/**
*
* @package phpBB Extension - phpBB Countdown
* Russian translation by HD321kbps
* @copyright (c) 2015 dmzx - http://www.dmzx-web.net
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
* @Author Stoker - http://www.phpbb3bbcodes.com
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	'COUNT_YEARS'				=> 'Год',
	'COUNT_MONTHS'				=> 'Месяцев',
	'COUNT_DAYS'				=> 'Дней',
	'COUNT_HOURS'				=> 'Часов',
	'COUNT_MINUTES'				=> 'Минут',
	'COUNT_SECONDS'				=> 'Секунд',
	'COUNT_DOWNCOUNT'			=> 'Вниз',
	'COUNT_UPCOUNT'				=> 'Вверх',
	'INSTALL_COUNTDOWN'					=> 'Установить счетчик обратного отсчета',
	'INSTALL_COUNTDOWN_CONFIRM'			=> 'Вы готовы установить расширение счетчик обратного отсчета?',
	'COUNTDOWN'							=> 'Счетчик обратного отсчета',
	'COUNTDOWN_EXPLAIN'					=> 'Устанавливая счетчик обратного отсчета, изменения базы даных будут внесены в автоматическом режиме UMIL.',
	'UNINSTALL_COUNTDOWN'				=> 'Удалить счетчик обратного отсчета',
	'UNINSTALL_COUNTDOWN_CONFIRM'		=> 'Вы готовы удалить расширение счетчик обратного отсчета? Все настройки и данные сохраненные в этом расширении будут удалены!',
	'UPDATE_COUNTDOWN'					=> 'Обновить счетчик обратного отсчета',
	'UPDATE_COUNTDOWN_CONFIRM'			=> 'Вы готовы обновить расширение счетчик обратного отсчета?',

	'ACP_COUNTDOWN_CONFIG'				=> 'Счетчик обратного отсчета',
	'ACP_COUNTDOWN_CONFIG_EXPLAIN'		=> 'Это страница конфигурации расширения счетчик обратного отсчета <a href="http://www.dmzx-web.net"><strong>dmzx</strong></a>. Автор Stoker.',
	'COUNTDOWN_VERSION'					=> 'Версия',
	'COUNTDOWN_DONATE'					=> 'Пожалуйста, от благодарите <a href="http://www.phpbb3bbcodes.com/donate.php"><strong>Пожертвование</strong></a> если вам понравилось расширение',
	'ACP_COUNTDOWN_CONFIG_SET'			=> 'Конфигурация',
	'COUNTDOWN_CONFIG_SAVED'			=> 'Настройки счетчика обратного отсчета сохранены',

	'COUNTDOWN_ENABLE'					=> 'Включить счетчик обратного отсчета',
	'COUNTDOWN_ENABLE_EXPLAIN'			=> 'Включить или отключить счетчик обратного отсчета здесь',
	'COUNTDOWN_DIRECTION' 				=> 'Направление счетчика обратного отсчета',
	'COUNTDOWN_DIRECTION_EXPLAIN'		=> 'Расширение счетчика обратного отсчета может считать вверх или вниз.',
	'COUNTDOWN_DATE' 					=> 'Дата счетчика обратного отсчета',
	'COUNTDOWN_DATE_EXPLAIN'			=> 'Пример: 2015/12/31 00:00:00',
	'COUNTDOWN_TEXT' 					=> 'Текст счетчика обратного отсчета',
	'COUNTDOWN_TEXT_EXPLAIN'			=> 'Этот текст будет показан прямо перед счетчиком обратного отсчета',
	'COUNTDOWN_COMPLETE'	 			=> 'Текст после завершения счетчика обратного отсчета',
	'COUNTDOWN_COMPLETE_EXPLAIN' 		=> 'Этот текст заметит счетчик обратного отсчета после его завершения',
	'COUNTDOWN_TESTMODE' 				=> 'Активировать тестовый режим',
	'COUNTDOWN_TESTMODE_EXPLAIN'		=> 'Если тестовый режим включен то только администраторы могут видеть счетчик обратного отсчета',
	'COUNTDOWN_YEAR'	 				=> 'Отображать количество лет',
	'COUNTDOWN_YEAR_EXPLAIN' 			=> 'Активируйте эту функцию, чтобы видеть количество лет в счетчике обратного отсчета',
	'COUNTDOWN_MONTH'	 				=> 'Отображать количество месяцев',
	'COUNTDOWN_MONTH_EXPLAIN' 			=> 'Активируйте эту функцию, чтобы видеть количество месяцев в счетчике обратного отсчета',
	'COUNTDOWN_OFFSET_ENABLE' 			=> 'Включить часовые пояса',
	'COUNTDOWN_OFFSET_ENABLE_EXPLAIN' 	=> 'Включить или выключить часовые пояса тут',
	'COUNTDOWN_OFFSET' 					=> 'Настройки часового пояса',
	'COUNTDOWN_OFFSET_EXPLAIN'			=> 'Если Вы хотите использовать определенный часовой пояс для всех пользователей, Вы можете указать его здесь.',
));
