<?php
/**
*
* acp_usu_install [Russian]
* @package Ultimate SEO URL phpBB SEO
* @version $$
* @copyright (c) 2006 - 2014 www.phpbb-seo.com
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/
/**
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//
$lang = array_merge($lang, array(
	// ACP
	'SEO_RELATED_TOPICS' => 'Related Topics',
	'SEO_RELATED' => 'Активация Related Topics',
	'SEO_RELATED_EXPLAIN' => 'Отображение списка не связанных тем, в разделе страницы.<br/><b style="color:red;">Внимание:</b><br/>Если MySQL >= 4.1 и раздел в таблице использует MyISAM, связанные темы будут получены с помощью полнотекстового индекса с заголовком раздела и будут отсортированы по релевантности. В других случаях, будет использоваться SQL LIKE, и результаты будут отсортированы по времени публикации',
	'SEO_RELATED_CHECK_IGNORE' => 'Игнорировать фильтр слов',
	'SEO_RELATED_CHECK_IGNORE_EXPLAIN' => 'Применить, или отменить, исключения при поиске смежных тем. В файле search_ignore_words.php',
	'SEO_RELATED_LIMIT' => 'Лимит связанных тем',
	'SEO_RELATED_LIMIT_EXPLAIN' => 'Максимальное количество связанных тем для отображения',
	'SEO_RELATED_ALLFORUMS' => 'Поиск во всех форумах',
	'SEO_RELATED_ALLFORUMS_EXPLAIN' => 'Поиск во всех форумах, вместо поиска в текущей форуме <br/> <b style="color:red;">. Внимание: </ B> <br/> Поиск во всех форумах немного медленнее и не обязательно принесет лучшие результаты',
	// Install
	'INSTALLED' => 'Модуль phpBB SEO Related Topics установлен',
	'ALREADY_INSTALLED' => 'Модуль phpBB SEO Related Topics уже установлен',
	'FULLTEXT_INSTALLED' => 'MySQL FullText Index установлен',
	'FULLTEXT_NOT_INSTALLED' => 'MySQL FullText Index не найден на сервере, вместо него будет использоваться SQL LIKE',
	'INSTALLATION' => 'Установка модуля phpBB SEO Related Topics',
	'INSTALLATION_START' => '&rArr; <a href="%1$s" ><b>Процесс установки модуля</b></a><br/><br/>&rArr; <a href="%2$s" ><b>Повторите попытку установить FullText Index</b></a> (MySQL >= 4.1 в разделе таблици используется только Myisam)<br/><br/>&rArr; <a href="%3$s" ><b>Процесс удаления модуля</b></a>',
	// un-install
	'UNINSTALLED' => 'Удаление модуля phpBB SEO Related Topics',
	'ALREADY_UNINSTALLED' => 'Модуль phpBB SEO Related Topics уже удален',
	'UNINSTALLATION' => 'Модуль phpBB SEO Related Topics удален',
	// SQL message
	'SQL_REQUIRED' => 'Пользователь базы данных не имеет достаточно привилегий для изменения таблиц, необходимо запустить этот запрос вручную, чтобы добавить или удалить MySQL FullText index :<br/>%1$s',
	// Security
	'SEO_LOGIN'		=> 'Зарегистрируйтесь и войдите в систему для просмотра этой страницы.',
	'SEO_LOGIN_ADMIN'	=> 'Войдите в систему как администратор, чтобы просмотреть эту страницу. <br/> Ваша сессия была уничтожена в целях безопасности.',
	'SEO_LOGIN_FOUNDER'	=> 'Войдите в систему как основатель для просмотра этой страницы.',
	'SEO_LOGIN_SESSION'	=> 'Проверка сессии не удалась.<br/>Настройки не изменены.<br/>Ваша сессия была уничтожена в целях безопасности.',
));
