<?php
/**
*
* info_acp_usu [Russian]
* @package Ultimate SEO URL phpBB SEO
* @version $$
* @copyright (c) 2006 - 2014 www.phpbb-seo.com
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/
/**
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//
$lang = array_merge($lang, array(
	'ACP_CAT_PHPBB_SEO' => 'SEO',
	'ACP_MOD_REWRITE' => 'Настройка трансляции адресов',
	'ACP_PHPBB_SEO_CLASS' => 'Настройка трансляции адресов',
	'ACP_FORUM_URL' => 'Управление адресами разделов',
	'ACP_REWRITE_CONF' => 'Настройки Сервера',
	'ACP_SEO_EXTENDED' => 'Расширенные настройки.',
	'ACP_SYNC_URL' => 'Синхронизация URL',
	'ACP_PREMOD_UPDATE' => '<h1>Объявление о новой версии</h1>
	                       <br />Данное обновление затрагивает только SEO-мод, но не ядро движка phpBB.<br /><br />Доступна новая версия SEO-мода: <b>%1$s</b>.<br /><br/> Посетите <a href="%2$s" title="Открыть в новом окне" target="_blank"><b>официальный форум</b></a> создателей мода для обновления установленной на вашем форуме версии SEO-мода.',
	'SEO_LOG_INSTALL_PHPBB_SEO' => 'phpBB SEO Ultimate SEO URL версии <b>%s</b> успешно установлен.',
	'SEO_LOG_INSTALL_PHPBB_SEO_FAIL' => 'Установка новой версии phpBB SEO Ultimate SEO URL не выполнена.<br/><br />%s',
	'SEO_LOG_UNINSTALL_PHPBB_SEO' => 'phpBB SEO Ultimate SEO URL версии <b>%s</b> успешно удален.',
	'SEO_LOG_UNINSTALL_PHPBB_SEO_FAIL' => 'Удаление phpBB SEO Ultimate SEO URL не выполнено.<br/><br />%s',
	'SEO_LOG_CONFIG_SETTINGS' => '<b>Обновлены настройки phpBB SEO Ultimate SEO URL.</b>',
	'SEO_LOG_CONFIG_FORUM_URL' => '<b>Обновлены адреса разделов форума.</b>',
	'SEO_LOG_CONFIG_HTACCESS' => '<b>Создан новый файл .htaccess.</b>',
	'SEO_LOG_CONFIG_EXTENDED' => '<b>Обновлены расширенные настройки phpBB SEO Ultimate SEO URL.</b>',
));
