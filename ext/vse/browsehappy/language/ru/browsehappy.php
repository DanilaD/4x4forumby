<?php
/**
*
* Browse Happy [English]
*
* @copyright (c) 2013 Matt Friedman
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'BROWSEHAPPY_TITLE'		=> 'Вы используете устаревший браузер!',
	'BROWSEHAPPY_MESSAGE'	=> 'Для безопасной и комфортной работы обновите версию вашего браузера.',
	'BROWSEHAPPY_UPGRADE'	=> 'Обновите ваш интернет браузер!',
	'BROWSEHAPPY_WARNING'	=> 'Предупреждение!',
));
